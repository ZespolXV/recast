//
//  AVCaptureState.swift
//  podcast
//
//  Created by Dawid Jenczewski on 03/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import AVFoundation

class AVCaptureState{
        static var isAudioDisabled: Bool {
               let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
               return status == .restricted || status == .denied
           }
}
