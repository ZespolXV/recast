//
//  Signaling.swift
//  podcast
//
//  Created by Dawid Jenczewski on 11/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import Starscream
//import SocketIO
import WebRTC;
import Speech

//Class holds all info about user
public class User{
    var name:String?
    var isHost:Bool?
    var connStatus:Bool?
    init(name: String,isHost: Bool,status :Bool){
        self.name = name
        self.isHost = isHost
        connStatus = status
    }
}

public class Sockets{
    public var state:Bool = true
    let socket:WebSocket?
    var rec:Recording?
    var otherUsername:String = ""
    public var sessionDir:URL?
    public var roomID:String = ""
    public var connectedUsers:[User] = []
    var username:String = UserDefaults.standard.value(forKey: "name") as? String ?? ""
    var hostDelegate:RoomsSignalDelegates?
    var joinCreateDelegate:JoinCreateDelegate?
    var rtcClient:RTCClient?
    enum MsgType: String {
        case ping = "ping"
        case login = "login"
        case offer = "offer"
        case answer = "answer"
        case candidate = "candidate"
        case createRoom = "createRoom"
        case joinRoom = "joinRoom"
        case roomIsFull = "roomIsFull"
        case userLogged = "userJoined"
        case leave = "leave"
        case recording = "recording"
        case getUsers = "getUser"
        case roomStatus = "status"
        case connLost = "connLost"
        case reconnect = "reconnect"
        case userReconnected = "userReconnected"
        case endGettingUsers = "endGettingUser"
        case hostRecv = "hostRecv"
        case error
        init(fromRawValue: String){
            self = MsgType(rawValue: fromRawValue) ?? .error
        }
        var description: String {
            return self.rawValue
        }
    }
    
    //function sending request for users in the room
    func getUsers(){
        let dict = ["type": "getUsers"]
        self.socket?.write(string: dict.json)
    }
    
    func setUsername(name: String){
        self.username = name
    }
    
    //function to login to room with name
    //types: create or join
    func Login(type: String!){
 
        let dict = ["type": type,
                    "room": self.roomID,
                    "name": self.username]
        if (socket!.isConnected){
            let jsonString = dict.json
            
            socket?.write(string: jsonString)
        }
        
    }
    //The function is performed after receiving a message from the server
    func onMessage(_ msg : String) -> Void {
        let dict = msg.dictionary
        if dict.isEmpty {
            
            return
        }
        guard let type = dict["type"] as? String else {
            
            return
        }
        //let success = dict["success"] as! String
        let msgType : MsgType = MsgType.init(rawValue: type)!
        switch msgType {
        case .ping:
            self.sendPong()
            break
        case .login:
            self.caseLogin(dict)
            break
        case .offer:
            self.caseOnOffer(dict)
            break
        case .answer:
            self.caseOnAnswer(dict)
            break
        case . candidate:
            self.caseOnCandidate(dict)
            break
            
        case .leave:
            self.caseOnLeave(dict)
            print("client left")
            break
            
        case .error:
            print("error")
            break
            
        case .createRoom:
            self.caseOnCreateRoom(dict)
            break
        case .joinRoom:
            self.caseOnJoinRoom(dict)
            break
        case .userLogged:
            self.caseLogged(dict)
            break
        case .recording:
            self.caseRecording(dict)
            break;
        case .getUsers:
            self.caseOnGetUsers(dict)
            break;
        case .roomStatus:
            self.caseOnStatus(dict)
            break
        case .connLost:
            self.caseOnConnLost(dict)
            break
        case .reconnect:
            self.caseOnRecoonect(dict)
            break
        case .userReconnected:
            self.caseOnUserReconnected(dict)
            break
        case .endGettingUsers:
            self.hostDelegate?.signal(UsersLoaded: self.connectedUsers)
            break
        case .roomIsFull:
            self.roomIsFullF(dict)
            break
        case .hostRecv:
            self.hostRecvFiles()
            break
        }
    }
    
    //Received when joining to room
    //tells us that the room is full
    func roomIsFullF(_ dict : [String : Any]){
        let state = dict["state"] as! Bool
        var msg = ""
        if(state){
         msg = "Room is full"
        }else{
            msg = ""
        }
        self.joinCreateDelegate?.signal(joinRoomStatus: false, withComunnicate: msg)
    }
    
    func hostRecvFiles(){
        self.hostDelegate?.signal(receiveAllFiles: true)
    }
    
    //To maintain an active connection
    func sendPong(){
        let dict = ["type": "pong"]
        self.socket?.write(string: dict.json)
    }
    
    //receive SDP offer
    func caseOnOffer(_ dict : [String : Any]) -> Void {
        
        let typ:RTCSdpType?
        let name = dict["name"] as! String
        let sdpDict = dict["offer"] as! [String : Any]
        let type = sdpDict["type"] as! String
        let sdp = sdpDict["sdp"] as! String
        if(type == "offer") {typ = .offer}
        else {typ = .answer}
        
        let rtcSessionDesc = RTCSessionDescription.init(type: typ!, sdp: sdp)
        self.otherUsername = name
        rtcClient?.createAnswerForOfferReceived(withRemoteSDP: rtcSessionDesc, roomID: self.roomID,name: name)
        
    }
    
    //someone joined to room
    func caseLogged(_ dict: [String: Any]) -> Void{
        let name = dict["name"] as! String
        //let isHost = dict["isHost"] as! Bool
        print("user" + name + " Logged")
        connectedUsers.append(User(name: name, isHost: false,status: true))
        self.hostDelegate?.signal(didJoin: connectedUsers)
        rtcClient?.makeOffer(name: name, roomID: roomID)
    }
    
    //receive users in room
    func caseOnGetUsers(_ dict: [String : Any]) -> Void{
        print("Jestem")
        let name = dict["name"] as! String
        let isHost = dict["isHost"] as! Bool
        let status = dict["status"] as! Bool // connected 1 or lostConnection 0
        connectedUsers.append(User(name: name, isHost: isHost,status: status))
        print("\(connectedUsers.count)sdsadsadsadsa")
    }
    
    //receive when joining to room
    //tells us that room is during recording, we cant join
    func caseOnStatus(_ dict: [String : Any]) -> Void{
        let status = dict["recording"] as! Bool
        if(status){
            let msg = "Cannot join, the room is recording"
            print(msg)
            self.joinCreateDelegate?.signal(joinRoomStatus: false, withComunnicate: msg)
        }
    }
    
    //receive message with success join to room with our usernames
    func caseLogin(_ dict : [String : Any]) -> Void {
        let isSuceess = dict["success"] as! Bool
        if (isSuceess == false){
            let msg = "Try a different name"
            print(msg)
            self.joinCreateDelegate?.signal(joinRoomStatus: false, withComunnicate: msg)
            //show alert
            print(dict)
        }else {
           print("Joined the room")
            self.joinCreateDelegate?.signal(joinRoomStatus: true, withComunnicate: "")
            let dict = ["type" : "userJoined",
                        "room" : roomID,
            "name" : username] as [String : Any]
            socket?.write(string: dict.json)
        }
        
        
    }
    //receive message about status of creating room
    func caseOnCreateRoom(_ dict : [String : Any]) ->Void{
        
        let isSuccess = dict["success"] as! Bool
        if(isSuccess == false){
            print("Pokój o tej nazwie już istnieje")
            self.joinCreateDelegate?.signal(createRoomStatus: false, withComunnicate: "Room with this name already exists")
        }else{
            print("Created room " + self.roomID)
            self.joinCreateDelegate?.signal(createRoomStatus: true, withComunnicate : "")
        }
    }
    //receive message about status of joining to room
    func caseOnJoinRoom(_ dict : [String : Any]) ->Void{
        let isSuccess = dict["success"] as! Bool
        if(isSuccess == false){
            let msg = "Didn't find room"
            print(msg)
            self.joinCreateDelegate?.signal(joinRoomStatus: false, withComunnicate: msg)
        }else{
        }
    }
    //receive ICECandidates
    func caseOnCandidate(_ dict : [String : Any]) -> Void {
        print(dict)
        let name = dict["name"] as! String
        let candidateDict = dict["candidate"] as! [String : Any]
        let mid = candidateDict["sdpMid"] as! String
        let index = candidateDict["sdpMLineIndex"] as! Int32
        let sdp = candidateDict["candidate"] as! String // check what tag it is coming
        let candidate : RTCIceCandidate = RTCIceCandidate.init(sdp: sdp, sdpMLineIndex: index, sdpMid: mid)
        //self.peerConnection?.add(candidate)
        for peer in self.rtcClient!.peerConnection!{
            if(peer.name == name)
            {
                guard let pC = peer.peerConnection else {return}
                rtcClient?.addIceCandidate(iceCandidate: candidate, peerConnection: pC)
            }
        }
    }
    //receive sdp answer for our offer
    func caseOnAnswer(_ dict : [String : Any]) -> Void {
        
        print(dict)
        let typ:RTCSdpType?
        let name = dict["name"] as! String
        let sdpDict = dict["answer"] as! [String : Any]
        let type = sdpDict["type"] as! String
        let sdp = sdpDict["sdp"] as! String
        if(type == "offer") {typ = .offer}
        else {typ = .answer}
        let rtcSessionDesc = RTCSessionDescription.init(type: typ!, sdp: sdp)
        rtcClient?.handleAnswerReceived(withRemoteSDP: rtcSessionDesc,name: name)
    }
    
    //receive message that we start,stop or pause recording
    func caseRecording(_ dict: [String : Any]) ->Void{
        print(dict)
        let type = dict["recording"] as! String
        let hostName = dict["name"] as! String
        if(username != hostName){
            for peer in self.rtcClient!.peerConnection!{
            if(peer.name == hostName){
                
                if(type == "Start"){
                    rec = Recording()
                    rec?.setDelegate(delegate: self.hostDelegate!)
                    rec?.roomID = self.roomID
                    print("recording")
                    AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
                        if hasPermission
                        {
                            print ("ACCEPTED")
                            }
                    }
                    
                    SFSpeechRecognizer.requestAuthorization { (permission) in
                        OperationQueue.main.addOperation {
                            switch permission{
                            case .authorized:
                                print("Authorized")
                            default:
                                print("NotAuthorized")
                            }
                        }
                    }
                    
                    //tu zaczynamy nagrywac
                    rec?.startRecording()
                    self.hostDelegate?.signal(recordChangeState: "Start")
                }else if(type == "Stop"){
                    //tu konczymy nagrywanie i wysylamy
                    rec?.stopRecording()
                    //self.sendFiles(peer:peer,rec:rec)
                    self.hostDelegate?.signal(recordChangeState: "Stop")
                }else if(type == "Pause")
                {
                    rec?.audioRecorder?.pause()
                    self.hostDelegate?.signal(recordChangeState: "Pause")
                }
                break;
            }
        }
        }else if(self.username == hostName){
            if(type == "Start"){
                rec = Recording()
                rec?.setDelegate(delegate: self.hostDelegate!)
                rec?.roomID = self.roomID
                rec?.sessionDir = self.sessionDir
                print("recording")
                AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
                    SFSpeechRecognizer.requestAuthorization{
                        (transcribePerm) in
                        if(hasPermission && transcribePerm == .authorized){
                            print("Accepted record and transcribe")
                            self.rec?.startRecording()
                        }
                        else{
                            print("Not accepted record or transcribe")
                        }
                    }
                }
                
                /*SFSpeechRecognizer.requestAuthorization { (permission) in
                    OperationQueue.main.addOperation {
                        switch permission{
                        case .authorized:
                            print("Authorized")
                        default:
                            print("NotAuthorized")
                        }
                    }
                }*/
                
                //tu zaczynamy nagrywac
               // rec?.startRecording()
                self.hostDelegate?.signal(recordChangeState: "Start")
            }else if(type == "Stop"){
                //tu konczymy nagrywanie
                rec?.stopRecording()
                self.hostDelegate?.signal(recordChangeState: "Stop")
            }else if(type == "Pause"){
                rec?.audioRecorder?.pause()
                self.hostDelegate?.signal(recordChangeState: "Pause")
            }
        }
    }
    
    //receive message when someone left from our room
    func caseOnLeave(_ dict: [String : Any]) ->Void{
        print(dict)
        let name = dict["name"] as! String
        let isHost = dict["isHost"] as! Bool
        print(name + " leave room")
        var index: Int = 0;
        var index2: Int = 0;
        //if leave client isnt host
        if(!isHost){
            //close connection where name == leave clientname
            print(self.rtcClient!.peerConnection!.count)
            for peer in self.rtcClient!.peerConnection!{
                    if(peer.name == name){
                        self.rtcClient?.disconnect(pConnection: peer.peerConnection)
                        peer.name = nil;
                        peer.roomID = nil;
                        self.rtcClient!.peerConnection!.remove(at: index)
                        index2 = 0
                        for user in self.connectedUsers{
                            if(user.name == name)
                            {
                                self.connectedUsers.remove(at: index2)
                               // self.hostDelegate?.signal(UserLeave: self.connectedUsers)
                            }
                            index2+=1
                        }
                    }
              index+=1
            }
            /*print(connectedUsers.count)
            for x in self.connectedUsers{
                print(x.name)
            }*/
        }else{
            // client is host (close all connection and room)
            for peer in self.rtcClient!.peerConnection!{
                    self.rtcClient?.disconnect(pConnection: peer.peerConnection)
                    peer.name = nil;
                    peer.roomID = nil;
                    index+=1
            }
            self.rtcClient?.peerConnection?.removeAll()
            self.connectedUsers.removeAll()
           // self.hostDelegate?.signal(UserLeave: self.connectedUsers)
        }
        self.state = false
        self.hostDelegate?.signal(UserLeave: self.connectedUsers)
    }
    
    //when someone lost connection
    func caseOnConnLost(_ dict: [String : Any]) -> Void{
        print(dict)
        let name = dict["name"] as! String
        let isHost = dict["isHost"] as! Bool
        var index = 0
        if(isHost){
            for peer in self.rtcClient!.peerConnection!{
                self.rtcClient?.disconnect(pConnection: peer.peerConnection)
                peer.name = nil
                peer.roomID = nil
            }
            self.rtcClient?.peerConnection?.removeAll()
        }else{
            for peer in self.rtcClient!.peerConnection!{
                if(peer.name == name){
                    self.rtcClient?.disconnect(pConnection: peer.peerConnection)
                    peer.name = nil
                    peer.roomID = nil
                    self.rtcClient?.peerConnection?.remove(at: index)
                    break
                }
                index+=1
            }
            //pauza nagrania
            rec?.audioRecorder?.pause()
        }
        self.hostDelegate?.signal(userLostConnection: User(name: name, isHost: isHost, status: false))
        let avs = AVAudioSession.sharedInstance()
        do{
            try avs.setMode(.voiceChat)
            try avs.setCategory(.playAndRecord, options: .mixWithOthers)
            try avs.setActive(true, options: .notifyOthersOnDeactivation)
        }catch{
            
        }
    }
    
    //when we reconnect successfull
    func caseOnRecoonect(_ dict: [String :Any]) ->Void{
        print("success reconnect")
        self.joinCreateDelegate?.signal(joinRoomStatus: true, withComunnicate: "")
    }
    
    //when someone in our room reconnect successfull
    func caseOnUserReconnected(_ dict: [String :Any]) ->Void{
        let name = dict["name"] as! String
        //tworzenie rozmowy od nowa
        self.rtcClient?.makeOffer(name: name, roomID: self.roomID)
        self.hostDelegate?.signal(userReconnect: User(name: name, isHost: false, status: true))
        print("UserReconnect")
    }
    
    //setting delegates
    func setDelegate(delegate: RoomsSignalDelegates){
        self.hostDelegate = delegate
    }
    func setJoinCreateDelegate(delegate : JoinCreateDelegate){
        self.joinCreateDelegate = delegate
    }
    
    //function to send files: recording m4a and transcription
    func sendFiles(/*peer : PeerIdentity,rec: Recording?*/){
        var peer:PeerIdentity?
        for user in self.connectedUsers{
            if(user.isHost!){
                for per in self.rtcClient!.peerConnection!{
                    if(per.name! == user.name){
                        peer = per
                    }
                }
            }
        }
        var soundData:Data = Data()
        var transcribeData:Data = Data()
        var timestampData:Data = Data()
        var wordsData:Data = Data()
        var durationData:Data = Data()
        var buff: RTCDataBuffer? = nil
        //sending m4a to host
        do{
            soundData = try Data(contentsOf: rec!.filepath!)
            buff = RTCDataBuffer.init(data: soundData, isBinary: true)
        }catch{
            print(error.localizedDescription)
            print("Blad z m4a")
        }
        peer!.dataChannels[0].sendData(buff!)
        print("Wyslalem " + (rec?.filename ?? ""))
        
        //sending transription to host
        do{
            transcribeData = try Data(contentsOf: rec!.txtFilePath!)
            buff = RTCDataBuffer.init(data: transcribeData, isBinary: true)
        }catch{
            print(error.localizedDescription)
            print("Blad z txt")
        }
        peer!.dataChannels[1].sendData(buff!)
        print("Wyslalem " + (rec?.txtFileName! ?? ""))
        
        //sending timestamps to host
        do{
            timestampData = try Data(contentsOf: rec!.timestampsFilePath!)
            buff = RTCDataBuffer.init(data: timestampData, isBinary: true)
        }catch{
            print(error.localizedDescription)
            print("Blad z timestamp")
        }
        peer!.dataChannels[2].sendData(buff!)
        print("Wyslalem timestamp")
        
        //sending word to host
        do{
            wordsData = try Data(contentsOf: rec!.wordsFilePath!)
            buff = RTCDataBuffer.init(data: wordsData, isBinary: true)
        }catch{
            print(error.localizedDescription)
            print("Blad z words")
        }
        peer!.dataChannels[3].sendData(buff!)
        print("Wyslalem words")
        
        //sending duration to host
        do{
            durationData = try Data(contentsOf: rec!.durationFilePath!)
            buff = RTCDataBuffer.init(data: durationData, isBinary: true)
        }catch{
            print(error.localizedDescription)
            print("Blad z words")
        }
        peer!.dataChannels[4].sendData(buff!)
        print("Wyslalem duration")
        
    }
    init(){
        var request = URLRequest(url: URL(string: "ws://recast.herokuapp.com/")!)
        request.timeoutInterval = 5
        self.socket = WebSocket(request: request)
        //self.socket = WebSocket(url: URL(string:/*"ws://192.168.0.2:8080"*/ "ws://recast.herokuapp.com/")!)
        self.socket?.delegate = self as WebSocketDelegate
        self.socket?.connect()
    }
    
    deinit {
        self.socket?.disconnect()
    }
}
extension Sockets : WebSocketDelegate {
    
    //MARK: WebsocketDelegate
    public func websocketDidConnect(socket: WebSocketClient) {
        print("connected")
    }
    
    public func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        //rozlaczanie rozmowy z wszystkimi
        if(rtcClient?.peerConnection != nil){
            if(!rtcClient!.peerConnection!.isEmpty){
                for peer in self.rtcClient!.peerConnection!{
                    self.rtcClient?.disconnect(pConnection: peer.peerConnection)
                    peer.name = nil
                    peer.roomID = nil
                }
                self.rtcClient?.peerConnection?.removeAll()
                if(state){
                    self.hostDelegate!.lostConnection()
                }
                state = true
            }else{
                self.joinCreateDelegate?.signal(createRoomStatus: false, withComunnicate: "Check your network connection")
                self.joinCreateDelegate?.signal(joinRoomStatus: false, withComunnicate: "Check your network connection")
            }
        }else{
            self.joinCreateDelegate?.signal(createRoomStatus: false, withComunnicate: "Check your network connection")
            self.joinCreateDelegate?.signal(joinRoomStatus: false, withComunnicate: "Check your network connection")
        }
        print("disconnected")
    }
    
    public func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        //print(text)
                if(text != "Hello world"){self.onMessage(text)}
    }
    
    public func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print(data)
    }
}
