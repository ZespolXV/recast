//
//  Connections.swift
//  podcast
//
//  Created by Dawid Jenczewski on 27/02/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import WebRTC;
import AVFoundation

//details about one connection
// peerConnection is p2p connection with user: name in room: roomID
// dataChannels sending files
public class PeerIdentity{
    public var name:String?
    public var roomID:String?
    public var peerConnection: RTCPeerConnection?
    public var dataChannels:[RTCDataChannel] = []
    public var iceServers:[RTCIceServer]?
    
    public func configure(defaultConnectionConstraint: RTCMediaConstraints,delegate: RTCPeerConnectionDelegate,connFactory:RTCPeerConnectionFactory){           initializePeerConnection(defaultConnectionConstraint: defaultConnectionConstraint, delegate: delegate,connFactory:connFactory)
    }
    
    //Initialize PeerConnection with configuration with ICEServers
    func initializePeerConnection(defaultConnectionConstraint: RTCMediaConstraints,delegate: RTCPeerConnectionDelegate,connFactory:RTCPeerConnectionFactory){
        let configuration = RTCConfiguration()
        configuration.iceServers = self.iceServers!
        self.peerConnection = connFactory.peerConnection(with: configuration, constraints: defaultConnectionConstraint, delegate: delegate)
        if(self.peerConnection == nil) {print("niestety")}
    }
    
}

public class RTCClient:NSObject
{
    public var amHost:Bool?
    public var recordDir:URL?
    public var filesCount:Int = 0
    public var filesCounter:Int = 0
    var roomDelegate:RoomsSignalDelegates?
    fileprivate var iceServers:[RTCIceServer] = []
    public var peerConnection:[PeerIdentity]? = []
    fileprivate var usersConnected:[String]?
    fileprivate var connFactory:RTCPeerConnectionFactory?
    fileprivate var vLocalStream:RTCMediaStream?
    fileprivate var audioTrack: RTCAudioTrack?
    fileprivate var candidates:[RTCIceCandidate] = []
    fileprivate var remoteICECandidates: [RTCIceCandidate] = []
    fileprivate var sdpType:String = ""
    public var channel:Sockets?
    fileprivate let audioCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true"], optionalConstraints: nil)
    var callConstraint:RTCMediaConstraints{
        return self.audioCallConstraint
    }
    fileprivate let defaultConnectionConstraint = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: ["DtlsSrtpKeyAgreement" : "true"])
    
    
    public override init(){
        super.init()
    }
    
    public convenience init(iceServers:[RTCIceServer])
    {
        self.init()
        self.iceServers = iceServers
        self.initializePeerConnectionFactory()
        self.setLocalStream()
    }

    deinit {
        
        for peer in self.peerConnection!{
            if let stream = peer.peerConnection!.localStreams.first {
                audioTrack = nil
                peer.peerConnection!.remove(stream)
            }
        }
        
    }
    
    func setDelegate(delegate: RoomsSignalDelegates){
        self.roomDelegate = delegate
    }
    
    func setLocalStream(){
        self.vLocalStream = self.localStream()
    }
    
    func initializePeerConnectionFactory(){
           RTCPeerConnectionFactory.initialize()
           self.connFactory = RTCPeerConnectionFactory()
       }
    
    //adding local audio stream to connection
    public func startConnection(pConnection : PeerIdentity) {
        guard let peerConnection = pConnection.peerConnection else {
            return
        }
        peerConnection.add(self.vLocalStream!)
    }
    
    // close voice connection
    public func disconnect(pConnection : RTCPeerConnection!) {
        guard let peerConnection = pConnection else {
            return
        }
        peerConnection.close()
        for peer in self.peerConnection!{
            if(peer.peerConnection == peerConnection)
            {
                peer.dataChannels.removeAll()
            }
        }
        if let stream = peerConnection.localStreams.first {
            audioTrack = nil
            peerConnection.remove(stream)
        }
    }
    
    //sending Session Description Protocol to signaling server
    func rtcClient(client : RTCClient, startCallWithSdp sdp: RTCSessionDescription,type : String,roomID :String?,name: String?){
        if (type == "offer") {
            
            let sdpDict : [String : String] = ["type" : type,
                                               "sdp" : sdp.sdp]
            let offerDict =  ["type" : "offer",
                              "offer" : sdpDict,
                              "room" : roomID!,
                              "name" : name!/*otherUsername*/] as [String : Any]
            client.channel?.socket?.write(string: offerDict.json)
            
            
        }else if (type == "answer") {
            
            let sdpDict : [String : String] = ["type" : type,
                                               "sdp" : sdp.sdp]
            
            let offerDict =  ["type" : "answer",
                              "answer" : sdpDict,
                              "room" : roomID!,
                              "name" : name!/*client.otherUsername*/] as [String : Any]
            
            client.channel?.socket?.write(string: offerDict.json)
            
        }
    }
    
    // sending ICE candidates to signaling server
    func rtcClient(pC : RTCPeerConnection, didGenerateIceCandidate: RTCIceCandidate){
        for peer in self.peerConnection!{
            if(peer.peerConnection == pC)
            {
                DispatchQueue.main.async {
                    //if (didGenerateIceCandidate != nil) {
                    
                        let candidate = ["candidate" : didGenerateIceCandidate.sdp,
                                         "sdpMid" : didGenerateIceCandidate.sdpMid as Any ,
                                     "sdpMLineIndex" : didGenerateIceCandidate.sdpMLineIndex] as [String : Any]
                        let candidateDict  = ["type" : "candidate",
                                              "name" : peer.name!,
                                              "room" : peer.roomID!,
                                              "candidate": candidate] as [String : Any]
                        self.channel?.socket?.write(string: candidateDict.json)
                    //}
                }
            }
        }
    }
    
    //making SDP as oferrer and sending to joined user
    //initialize PeerIdentity
    public func makeOffer(name : String?,roomID :String?) {
        let peer:PeerIdentity = PeerIdentity()
        peer.name = name
        peer.roomID = roomID
        peer.iceServers = self.iceServers
        peer.configure(defaultConnectionConstraint: defaultConnectionConstraint, delegate: self,connFactory: self.connFactory!)
        self.startConnection(pConnection: peer)
        self.peerConnection?.append(peer);
        
        guard let peerConnection = peer.peerConnection else {
            return
        }
        //print(peer.peerConnection.debugDescription)
        //print(peer.peerConnection?.localStreams.debugDescription)
        //
        if(self.amHost!){
            self.openDataChannel(peer: peer, filename: name! + ".m4a")
            self.openDataChannel(peer: peer, filename: name! + ".txt")
            self.openDataChannel(peer: peer, filename: name! + ".timestamps.txt")
            self.openDataChannel(peer: peer, filename: name! + ".words.txt")
            self.openDataChannel(peer: peer, filename: name! + ".duration.txt")
        }
        peerConnection.offer(for: self.callConstraint, completionHandler: { [weak self]  (sdp, error) in
            guard let this = self else { return }
            if let error = error {
                print("blad")
                print(error.localizedDescription)
            } else {
                this.sdpType = "offer"
                print(sdp!.sdp)
                this.handleSdpGenerated(sdpDescription: sdp,roomID: roomID,name:name)
            }
        })
    }
    
    //handle answer and save as remoteSDP
    public func handleAnswerReceived(withRemoteSDP remoteSdp: RTCSessionDescription?,name : String?) {
        guard let remoteSdp = remoteSdp else {
            return
        }
        for peer in self.peerConnection! {
        // Add remote description
            if(peer.name == name)
            {
                print("tu tez")
                guard let peerConnection = peer.peerConnection else{
                    print("blad")
                    return
                }
                let sessionDescription = RTCSessionDescription.init(type: .answer, sdp: remoteSdp.sdp)
                
                peerConnection.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
                    guard let this = self else { return }
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        this.handleRemoteDescriptionSet(peer: peer)
                        //this.state = .connected
                    }
                })
                break;
            }
        }
    }
    
    //handle offer, save and create answer
    public func createAnswerForOfferReceived(withRemoteSDP remoteSdp: RTCSessionDescription?,roomID :String?,name: String?) {
        guard let remoteSdp = remoteSdp else {
                return
        }

        // Add remote description
        let peer:PeerIdentity = PeerIdentity()
               peer.name = name
               peer.roomID = roomID
        peer.iceServers = self.iceServers
        peer.configure(defaultConnectionConstraint: defaultConnectionConstraint, delegate: self,connFactory: self.connFactory!)
               self.startConnection(pConnection: peer)
               self.peerConnection?.append(peer);
        print("count" + " \(self.peerConnection!.count)")
                let sessionDescription = RTCSessionDescription(type: .offer, sdp: remoteSdp.sdp)
                
                guard let peerConnection = peer.peerConnection else{
                    return
                }
                peerConnection.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
                    guard let this = self else { return }
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        this.handleRemoteDescriptionSet(peer: peer)
                        // create answer
                        peerConnection.answer(for: this.callConstraint, completionHandler:
                            { (sdp, error) in
                                if let error = error {
                                    print(error.localizedDescription)
                                } else {
                                    this.sdpType = "answer"
                                    this.handleSdpGenerated(sdpDescription: sdp,roomID:roomID,name: name)
                                   // this.state = .connected
                                }
                        })
                    }
                })
    }
    
    
    
    //if we have remote sdp then set iceCandidate to connection
    //else append to remoteIceCandidate array
    public func addIceCandidate(iceCandidate: RTCIceCandidate,peerConnection: RTCPeerConnection) {
           // Set ice candidate after setting remote description
           if peerConnection.remoteDescription != nil {
               peerConnection.add(iceCandidate)
           } else {
               self.remoteICECandidates.append(iceCandidate)
           }
       }

       public func muteCall(_ mute: Bool) {
           self.audioTrack?.isEnabled = !mute
       }    
}

public struct ErrorDomain {
    public static let videoPermissionDenied = "Video permission denied"
    public static let audioPermissionDenied = "Audio permission denied"
}

private extension RTCClient{
    
    //add ICE Candidates to PeerConnection after handle remote description
    func handleRemoteDescriptionSet(peer: PeerIdentity){
        guard let peerConnection = peer.peerConnection else{
            return
        }
        for iceCandidate in self.remoteICECandidates
        {
            peerConnection.add(iceCandidate)
        }
        self.remoteICECandidates = []
    }
    
    //get audioTracks and add to local audio stream
    func localStream() -> RTCMediaStream?{
        guard let factory = self.connFactory else{
            return nil
        }
        let localStream = factory.mediaStream(withStreamId: "RTCmS")
        if !AVCaptureState.isAudioDisabled{
            let audioTrack = factory.audioTrack(withTrackId: "RTCaS0")
            self.audioTrack = audioTrack
            audioTrack.source.volume=5
            localStream.addAudioTrack(audioTrack)            
            
        }else{
            let error = NSError.init(domain: "Audio permission denied" , code: 0, userInfo: nil)
            print(error)
        }
        return localStream
    }
    
    //when sdp was generated
    func handleSdpGenerated(sdpDescription: RTCSessionDescription?, roomID:String?, name: String?) {
           guard let sdpDescription = sdpDescription  else {
                print("blad")
               return
           }
           // set local description
        for peer in self.peerConnection!{
            if(peer.name == name){
                
                guard let peerConnection = peer.peerConnection else{
                    print ("blad")
                    return
                }
                peerConnection.setLocalDescription(sdpDescription, completionHandler: { (error) in
                    // issue in setting local description
                    guard let error = error else { return }
                    print(error.localizedDescription)
                     })
                break;
            }
        }
            print("przeszlo")
           //  Signal to server to pass this sdp with for the session call
        self.rtcClient(client: self, startCallWithSdp: sdpDescription,type: sdpType,roomID: roomID,name: name)
       }
}

extension RTCClient{
    // open and configure dataChannels
    public func openDataChannel(peer:PeerIdentity,filename:String){
        let configuration = RTCDataChannelConfiguration()
        //configuration.channelId = 1
        //configuration.maxRetransmits = 30
        //configuration.maxPacketLifeTime = 30000
        let dataChannel = peer.peerConnection!.dataChannel(forLabel: filename, configuration: configuration)
        dataChannel.delegate = self as RTCDataChannelDelegate
        print(dataChannel.debugDescription)
        peer.dataChannels.append(dataChannel)
        print(peer.dataChannels[0].debugDescription)
        //self.dataChannel
    }
    public func getData(){
        
    }
}

extension RTCClient:RTCDataChannelDelegate{
    public func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        if(dataChannel.readyState == .closing)
        {
            print("Host zamknal kanal \(dataChannel.label)")
            dataChannel.close()
        }
    }
    
    public func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        print("Otrzymalem plik")
        let data:Data = buffer.data
        let filename = dataChannel.label
        do{
            try data.write(to: recordDir!.appendingPathComponent(filename))
            print("zapisalem plik do \(recordDir!.appendingPathComponent(filename))")
        }catch{
            print("Blad z zapisaniem odebranego pliku \(filename)" )
        }
        //dataChannel.close()
        print("Odebralem i zamknalem kanal: \(filename)")
        filesCounter += 1
        if(filesCounter == filesCount){
            self.roomDelegate?.signal(receiveAllFiles: true)
        }
    }
    public func dataChannel(_ dataChannel: RTCDataChannel, didSendMessageWith bufer: RTCDataBuffer){
    }
    
}

extension RTCClient: RTCPeerConnectionDelegate {
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
    }
    

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        if(stream.audioTracks.count>0){
            stream.audioTracks[0].source.volume = 5
        }
    }

    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {

    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        //self.delegate?.rtcClient(client: self, didChangeConnectionState: newState)
        if(newState == .failed){
            print("failed state")
            // tu lostconn
            self.roomDelegate?.lostConnection()
        }
        print(newState.rawValue)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        //self.candidates.append(candidate)
        self.rtcClient(pC: peerConnection, didGenerateIceCandidate: candidate)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        for peer in self.peerConnection!{
            if(peer.peerConnection == peerConnection){
                print("Zalozono kanal" + dataChannel.label)
                peer.dataChannels.append(dataChannel)
            }
        }
    }
    public func peerConnection(_ peerConnection: RTCPeerConnection!, didCreateSessionDescription sdp: RTCSessionDescription!) {
        
    }

}

