//
//  Room.swift
//  podcast
//
//  Created by Dawid Jenczewski on 23/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import WebRTC;
import Starscream


public class Room{
    fileprivate var defaultChannel:Sockets
    fileprivate var roomID: String?
    fileprivate var rtcClient:RTCClient
    fileprivate var avs:AVAudioSession?
    let iceServer:RTCIceServer = RTCIceServer(urlStrings: ["stun:stun.l.google.com:19302"])
    let iceServer2:RTCIceServer = RTCIceServer(urlStrings: ["turn:192.158.29.39:3478?transport=tcp"], username: "28224511:1379330808", credential: "JZEOEt2V3Qb0y27GRntt2u2PAYA=")
    
    init(){
        self.defaultChannel = Sockets()
        self.rtcClient = RTCClient(iceServers: [iceServer,iceServer2])
    }
    
    
    public func getUsers() -> [User]{
        self.defaultChannel.getUsers()
        return self.defaultChannel.connectedUsers
    }
    
    //function to make room
    // make signaling channel
    public func createRoom(roomID : String?){
        self.defaultChannel.socket?.onConnect = {
            //self.defaultChannel.socket?.onDid
            if(self.defaultChannel.socket!.isConnected ?? false){
                self.defaultChannel.roomID = roomID ?? ""
                self.rtcClient.amHost = true
                self.rtcClient.channel = self.defaultChannel
                self.defaultChannel.rtcClient = self.rtcClient
                self.defaultChannel.Login(type: "createRoom")
            }else{
                print("Brak ci neta biedaku")
            }
        }
        /*while(!(self.defaultChannel.socket?.isConnected ?? true))
        {
            usleep(500)
        }*/
        
    }
    //function to join room
    // make signaling channel
    public func joinRoom(roomID : String?){
        self.defaultChannel.socket?.onConnect = {
            //self.defaultChannel.socket?.onDid
            if(self.defaultChannel.socket!.isConnected ?? false){
                self.defaultChannel.roomID = roomID ?? ""
                self.rtcClient.amHost = false
                self.rtcClient.channel = self.defaultChannel
                self.defaultChannel.rtcClient = self.rtcClient
                self.defaultChannel.Login(type: "joinRoom")
            }else{
                print("Brak ci neta biedaku")
            }
        }
        
    }
    //sending messages to server about start,stop or pause recording
    public func sendStartRecording(){
        let dict = ["type" : "recording",
                    "recording" : "Start"]
        self.defaultChannel.socket?.write(string: dict.json)
    }
    public func sendStopRecording(){
        let dict = ["type" : "recording",
                    "recording" : "Stop"]
        self.defaultChannel.socket?.write(string: dict.json)
    }
    public func sendPauseRecording(){
        let dict = ["type" : "recording",
                    "recording" : "Pause"]
        self.defaultChannel.socket?.write(string: dict.json)
    }
    public func sendLeave(){
        let dict = ["type":"leave"]
        self.defaultChannel.socket?.write(string: dict.json)
    }
    //when we leave room
    //close all voice connections
    public func leaveRoom(){
        sendLeave()
        
        for peer in self.rtcClient.peerConnection!{
            self.rtcClient.disconnect(pConnection: peer.peerConnection!)
        }
        
        self.rtcClient.peerConnection?.removeAll()
        self.defaultChannel.socket?.disconnect()
    }
    func setDelegate(delegate: RoomsSignalDelegates){
        self.defaultChannel.setDelegate(delegate: delegate)
        self.rtcClient.setDelegate(delegate: delegate)
    }
    func setJoinCreateDelegate(delegate: JoinCreateDelegate){
        self.defaultChannel.setJoinCreateDelegate(delegate: delegate)
    }
    func setUsername(name: String)
    {
        self.defaultChannel.setUsername(name: name)
    }
    func setSessionDir(sessionDir:URL){
        self.rtcClient.recordDir = sessionDir
        self.defaultChannel.sessionDir = sessionDir
    }
    func sendFiles(){
        self.defaultChannel.sendFiles()
    }
    func sendHostRecvAllFiles(){
        let dict = ["type" : "hostRecv"]
        self.defaultChannel.socket?.write(string: dict.json)
    }
    func setFilesCount(){
        self.rtcClient.filesCount = self.defaultChannel.connectedUsers.count * 5
        self.rtcClient.filesCounter = 0
    }
    func lostConnection(){
        for peer in self.rtcClient.peerConnection!{
            self.rtcClient.disconnect(pConnection: peer.peerConnection!)
        }
        
        self.rtcClient.peerConnection?.removeAll()
        self.defaultChannel.state = false
        self.defaultChannel.socket?.disconnect()
    }
}
