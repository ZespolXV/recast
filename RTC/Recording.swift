//
//  Recording.swift
//  podcast
//
//  Created by Pawel on 04/04/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Speech

public class Recording{
        
    var recordingSession:AVAudioSession?
    var audioRecorder:AVAudioRecorder?
    var audioPlayer:AVAudioPlayer?
    var audioEngine = AVAudioEngine()
    
    private var recognitionRequest:SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask:SFSpeechRecognitionTask?
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: UserDefaults.standard.string(forKey: "language")!))
    public var txtFileName:String?
    public var txtFilePath:URL?
    private var transcribeString:String = ""
    
    //Saving timestamps and coresponding to them words
    private var transcriptionSegments = [SFTranscriptionSegment]()
    public var timestampsFilePath:URL?
    public var durationFilePath:URL?
    public var wordsFilePath:URL?
    public var finishedRecording = false
    var delegate:RoomsSignalDelegates?
    
    public var name:String?
    var roomID:String?
    public var filepath:URL?
    public var filename:String?
    public var sessionDir:URL?

   
    func setDelegate(delegate:RoomsSignalDelegates){
        self.delegate = delegate
    }
    
    public func startRecording() {
        //Check if we have an active recorder
        recordingSession = AVAudioSession.sharedInstance()
        do{
            try recordingSession?.setCategory(.playAndRecord, mode: .voiceChat, options: .duckOthers)
            try recordingSession?.setActive(true, options: .notifyOthersOnDeactivation)
        }catch{
            print("Error with setting audioSession to record")
        }
        if audioRecorder == nil {
            name = UserDefaults.standard.value(forKey: "name") as? String
            if(sessionDir == nil) {
                sessionDir = getDirectory()
            }
            filename = name! + ".m4a"
            filepath = sessionDir!.appendingPathComponent(filename!)
            //Making file for transcription
            txtFileName = name! + ".txt"
            txtFilePath = sessionDir!.appendingPathComponent(txtFileName!)
            
            //Making path for timestamps
            let timestampsFileName = name! + ".timestamps.txt"
            timestampsFilePath = sessionDir!.appendingPathComponent(timestampsFileName)
            
            //Making path for timestamps_last
            let durationFileName = name! + ".duration.txt"
            durationFilePath = sessionDir!.appendingPathComponent(durationFileName)
            
            //Making path for words(timestamps)
            let wordsFileName = name! + ".words.txt"
            wordsFilePath = sessionDir!.appendingPathComponent(wordsFileName)
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 12000, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue ]
            
            // Start audioudio recording
            do {
                audioRecorder = try AVAudioRecorder(url: filepath!, settings: settings)
                audioRecorder!.record()
                transcribe()
                print("Start recording")
            } catch {
                print("ERROR: \(error)")
            }
        } else {
            recognitionTask?.cancel()
            self.recognitionTask = nil
            
            audioRecorder!.record()
            transcribe()
        }
    }
    
    func transcribe(){
        //Make sure that recognitionTask is nil
        recognitionTask?.cancel()
        self.recognitionTask = nil
        
        //Catching words from audio
        let inputNode = audioEngine.inputNode
        inputNode.removeTap(onBus: 0)
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        //Start audio engine
        do {
            audioEngine.prepare()
            try audioEngine.start()
        } catch {
            print("ERROR: \(error)")
        }
        
        //Prepare buffer
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest?.taskHint = .dictation
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create SFSpeechAudioBufferRecognitionRequest!")
        }
        recognitionRequest.shouldReportPartialResults = true
        
        if #available(iOS 13, *){
            if speechRecognizer?.supportsOnDeviceRecognition ?? false {
                recognitionRequest.requiresOnDeviceRecognition = true
            }
        }
        
        //Transcribe
        speechRecognizer?.defaultTaskHint = .dictation
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest) { result, error in
            //Save transcription to string
            if let trans = result?.bestTranscription {
                if result!.isFinal == true{
                    //Save speech as full string
                    self.transcribeString = trans.formattedString
                    self.transcriptionSegments = trans.segments                        //MARK:ZAPISYWANIE SEGMENTOW
                    self.saveTranscriptionToFiles()
                    print("ZAPISANE")
                    self.delegate?.signal(TransState: true)
                }
            }
            //Print errors
            if error != nil {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
            }
        }
    }
    
    private func saveTranscriptionToFiles(){
        var tsString = [String]()
        var valueString = [String]()
        var substringArray = [String]()
        
        //Read parts of transcription segments to arrays
        for seg in transcriptionSegments {
            tsString.append(String(describing: seg.timestamp))
            substringArray.append(seg.substring)
            valueString.append(String(describing: seg.duration))
        }
        
        //Save arrays to files
        do {
            try transcribeString.write(to: txtFilePath!, atomically: true, encoding: .utf8)
            try tsString.joined(separator: "-").write(to: timestampsFilePath!, atomically: true, encoding: .utf8)
            try substringArray.joined(separator: "-").write(to: wordsFilePath!, atomically: true, encoding: .utf8)
            try valueString.joined(separator: "-").write(to: durationFilePath!, atomically: true, encoding: .utf8)
            
            print("Timestamps: \(tsString.joined(separator: "-"))")
            print("Duration: \(valueString.joined(separator: "-"))")
            print("Words: \(substringArray.joined(separator: "-"))")
        } catch {
            print("ERRRO: \(error)")
        }
    }
    
    //Stopping audio recording
    public func stopRecording(){
        audioRecorder!.stop()
        audioRecorder = nil
        
        //Save string to file
        recognitionRequest?.endAudio()
        audioEngine.stop()
        recognitionTask?.finish()
    }

    init(){
        
    }
        
        
// Function that gets path to directory
    
    func getDirectory()-> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        return documentDirectory
    }
    
}

