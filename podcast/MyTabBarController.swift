//
//  MyTabBarController.swift
//  podcast
//
//  Created by Karol on 19/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit

class MyTabBarController: UITabBarController{
    override func viewDidLoad() {
        self.selectedIndex = 1
    }
}
