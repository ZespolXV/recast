//
//  RecordRoomController.swift
//  podcast
//
//  Created by Karol on 19/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit
import WebRTC
protocol RoomsSignalDelegates {
    func signal(didJoin user: [User])//ok
    func signal(UsersLoaded users: [User])//ok
    func signal(UserLeave users:[User])//ok
    func signal(userLostConnection user: User)//ok // rozłaczyło kogoś w pokoju
    func lostConnection() // rozłaczyło nas
    func signal(userReconnect user: User)//ok // ktos powrócił do pokoju
    func signal(recordChangeState state:String) // mozliwe statusy "Start" "Stop" "Pause"
    func signal(TransState state:Bool)
    func signal(receiveAllFiles state: Bool)
}

class RecordRoomController: UIViewController,RoomsSignalDelegates{
    
    func signal(receiveAllFiles state: Bool) {
        print("Odebralem wszystkie pliki")
        self.room?.sendHostRecvAllFiles()
        dismiss(animated: false, completion: nil)
        }
        
        func signal(TransState state: Bool) {
            print("ZAKONCZENIE TRANSKRYPCJI")
            
               dismiss(animated: false, completion: nil)
               
               if(connectedUsers != nil)
               {
               let alert = UIAlertController(title: "Alert", message: "Receiving files.", preferredStyle: .alert)
                       alert.setTintColor(newColor: .white)
                       alert.setTitleColor(newColor: .systemRed)
                       alert.setMessageColor(newColor: .systemRed)
                       alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))

                       present(alert, animated: true, completion: nil)
               }else{
                
            }
             
    }
    

    @IBOutlet weak var statusLabel: UILabel!
    var secondVC : UIViewController?
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    func signal(recordChangeState state: String) {
        if(state == "Start"){
            //bossname.text = "Test: trwa nagrywanie"
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownAction), userInfo: nil, repeats: true)
            print("SYGNAL NAGRYWANIE")
        }else if(state == "Stop"){
            //bossname.text = "Test: nagrywanie stop"
            let alert = UIAlertController(title: "Alert", message: "Wait for transctiption to finish.", preferredStyle: .alert)
                    alert.setTintColor(newColor: .white)
                    alert.setTitleColor(newColor: .systemRed)
                    alert.setMessageColor(newColor: .systemRed)
                    alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
                         
                    present(alert, animated: true, completion: nil)
            timer.invalidate()
            countdown = 0
        }else{
            //bossname.text = "Test: trwa pauza"
            timer.invalidate()
        }
    }
    
    func signal() {
        
    }
    
    var connectedUsers:[User]?
    var index:Int = 0
    func signal(userLostConnection user: User) {
        var pomoc = 0
        var loserIndex = 0
        for x in labels{
            if(x.text == user.name){
                loserIndex = pomoc
            }
            pomoc = pomoc+1
        }
        views[loserIndex].layer.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.3607843137, blue: 0.3607843137, alpha: 1)
        if #available(iOS 13.0, *) {
            images2[loserIndex].image = UIImage(systemName: "speaker.zzz")
        } else {
            // Fallback on earlier versions
        }
        //bossname.text = "eloo"
        
    }
    
    func lostConnection() {
        self.room?.lostConnection()
        let alert = UIAlertController(title: "Connection lost", message: "Your connection have been terminated. Room is closed.", preferredStyle: .alert)
        alert.setTintColor(newColor: .white)
        alert.setTitleColor(newColor: .systemRed)
        alert.setMessageColor(newColor: .systemRed)
        alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
            
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {action in
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "menu")
            self.present(newViewController, animated: true, completion: nil)
            
            
        }))
            present(alert, animated: true, completion: nil)
        return;
    }
    func goToMain(){
        
    }
    func signal(userReconnect user: User) {
            var pomoc = 0
            var loserIndex = 0
            for x in labels{
                if(x.text == user.name){
                    loserIndex = pomoc
                }
                pomoc = pomoc+1
            }
           views[loserIndex].layer.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
            if #available(iOS 13.0, *) {
                images2[loserIndex].image = UIImage(systemName: "speaker.3")
            } else {
                // Fallback on earlier versions
            }
            //bossname.text = "powrot"
    }
    var views = [UIView]()
    var labels = [UILabel]()
    var images = [UIImageView]()
    var images2 = [UIImageView]()
    
    var UsersCounter: Int = 0
    
    
    func signal(UserLeave users: [User]) {
        //bossname.text = "wyszedl"
        for x in 0...users.count{
            views[x].removeFromSuperview()
            labels[x].removeFromSuperview()
            images[x].removeFromSuperview()
            images2[x].removeFromSuperview()
        }
        views.removeAll()
        labels.removeAll()
        images.removeAll()
        images2.removeAll()
        
        UsersCounter = users.count
        connectedUsers = users

        var i = 0
        var pion = 16, poziom = 195, wysokosc = 50
        var pion2 = 100, poziom2 = 205, wysokosc2 = 24
        var pion3 = 25, poziom3 = 200, wysokosc3 = 19
        var pion4 = 300, poziom4 = 200, wysokosc4 = 19
        
        //Do testow na sztywno 3
        let pomoc = (connectedUsers!.count)
        //let pomoc = 3
        for _ in 0...pomoc{
            views.append(UIView())
            labels.append(UILabel())
            images.append(UIImageView())
            images2.append(UIImageView())
        }
        
        while pomoc>i {
            if(connectedUsers?[i].isHost == false){
            views[i] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 50))
            //views[i].layer.backgroundColor = UIColor.gray.cgColor
            
            labels[i] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 25))
            labels[i].text = connectedUsers?[i].name
            
                images[i] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
                images2[i] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 40, height: 40))

                images[i].tintColor = UIColor.white
                images2[i].tintColor = UIColor.white
                if #available(iOS 13.0, *) {
                    images2[i].image = UIImage(systemName: "speaker.3")
                } else {
                    // Fallback on earlier versions
                }
                if #available(iOS 13.0, *) {
                    images[i].image = UIImage(systemName: "person.circle.fill")
                } else {
                    // Fallback on earlier versions
                }
                views[i].layer.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
                labels[i].textAlignment = .left
                labels[i].textColor = .white
                views[i].layer.cornerRadius = 15
            
            
            mainView.addSubview(views[i])
            mainView.addSubview(labels[i])
            mainView.addSubview(images[i])
            mainView.addSubview(images2[i])
            
            poziom = poziom+wysokosc+5
            poziom2 = poziom2+wysokosc+5
            poziom3 = poziom3+wysokosc+5
            poziom4 = poziom4+wysokosc+5
        }
            i = i+1
        }
        
        
    }
    
    func signal(didJoin user: [User]) {
        UsersCounter = user.count
        var x = 0
        while x  < user.count - 1{
            views[x].removeFromSuperview()
            labels[x].removeFromSuperview()
            images[x].removeFromSuperview()
            images2[x].removeFromSuperview()
            x += 1
        }
        views.removeAll()
        labels.removeAll()
        images.removeAll()
        images2.removeAll()
        //print("ktos przyszedl")
        //bossname.text = "join"
        connectedUsers = user
        //bossname.text = String(user.count)
        var i = 0
        var pion = 16, poziom = 195, wysokosc = 50
        var pion2 = 100, poziom2 = 205, wysokosc2 = 24
        var pion3 = 25, poziom3 = 200, wysokosc3 = 19
        var pion4 = 300, poziom4 = 200, wysokosc4 = 19
        
        //Do testow na sztywno 3
        let pomoc = (connectedUsers!.count)
        //let pomoc = 3
        for _ in 1...pomoc{
            views.append(UIView())
            labels.append(UILabel())
            images.append(UIImageView())
            images2.append(UIImageView())
        }
        
        while pomoc>i {
            if(connectedUsers?[i].isHost == false){
            views[i] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 50))
            //views[i].layer.backgroundColor = UIColor.gray.cgColor
            
            labels[i] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 25))
            labels[i].text = connectedUsers?[i].name
            
                images[i] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
                images2[i] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 40, height: 40))

                images[i].tintColor = UIColor.white
                images2[i].tintColor = UIColor.white
                if #available(iOS 13.0, *) {
                    images2[i].image = UIImage(systemName: "speaker.3")
                } else {
                    // Fallback on earlier versions
                }
                if #available(iOS 13.0, *) {
                    images[i].image = UIImage(systemName: "person.circle.fill")
                } else {
                    // Fallback on earlier versions
                }
                views[i].layer.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
                labels[i].textAlignment = .left
                labels[i].textColor = .white
                views[i].layer.cornerRadius = 15
            
            
            mainView.addSubview(views[i])
            mainView.addSubview(labels[i])
            mainView.addSubview(images[i])
            mainView.addSubview(images2[i])
            
            poziom = poziom+wysokosc+5
            poziom2 = poziom2+wysokosc+5
            poziom3 = poziom3+wysokosc+5
            poziom4 = poziom4+wysokosc+5
        }
            i = i+1
        }
        
        
        print("ktos przydszedl")
        //print("UZUMYMW")
    }
    func signal(UsersLoaded users: [User]){
        UsersCounter = users.count
       connectedUsers = users
        // UsersCounter = users.count
               //print("ktos przyszedl")
               //bossname.text = "join"
               connectedUsers = users
               //bossname.text = String(user.count)
               
               var i = 0
               var pion = 16, poziom = 195, wysokosc = 50
               var pion2 = 100, poziom2 = 205, wysokosc2 = 24
               var pion3 = 25, poziom3 = 200, wysokosc3 = 19
               var pion4 = 300, poziom4 = 200, wysokosc4 = 19
               
               //Do testow na sztywno 3
               let pomoc = (connectedUsers!.count)
               //let pomoc = 3
               for _ in 1...pomoc{
                   views.append(UIView())
                   labels.append(UILabel())
                   images.append(UIImageView())
                   images2.append(UIImageView())
               }
               
               while pomoc>i {
                   if(connectedUsers?[i].isHost == false){
                   views[i] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 50))
                   //views[i].layer.backgroundColor = UIColor.gray.cgColor
                   
                   labels[i] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 25))
                   labels[i].text = connectedUsers?[i].name
                   
                       images[i] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
                       images2[i] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 40, height: 40))

                images[i].tintColor = UIColor.white
                images2[i].tintColor = UIColor.white
                if #available(iOS 13.0, *) {
                    images2[i].image = UIImage(systemName: "speaker.3")
                } else {
                    // Fallback on earlier versions
                }
                if #available(iOS 13.0, *) {
                    images[i].image = UIImage(systemName: "person.circle.fill")
                } else {
                    // Fallback on earlier versions
                }
                views[i].layer.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
                labels[i].textAlignment = .left
                labels[i].textColor = .white
                views[i].layer.cornerRadius = 15
            
            
            mainView.addSubview(views[i])
            mainView.addSubview(labels[i])
            mainView.addSubview(images[i])
            mainView.addSubview(images2[i])
            
            poziom = poziom+wysokosc+5
            poziom2 = poziom2+wysokosc+5
            poziom3 = poziom3+wysokosc+5
            poziom4 = poziom4+wysokosc+5
        }
            i = i+1
        }
        
    }
    var room:Room?
    var indexOfSession:Int = 0
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var roomname: UILabel!
    
    @IBOutlet weak var bossname: UILabel!
    
    @IBOutlet weak var bossView: UIView!
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var roomView: UIView!
    
    var timer: Timer!
    var countdown: Int = 0
    var hour: String = ""
    @objc func countDownAction(){
        countdown += 1
        if(countdown%60 < 10){
            hour = "\(countdown/60):0\(countdown%60)"
        }
        else
        {
            hour = "\(countdown/60):\(countdown%60)"
        }
        timerLabel.text = hour
    }
    override func viewDidAppear(_ animated: Bool){
        print("Ala")
        secondVC?.dismiss(animated: false, completion: nil)
        statusView.layer.cornerRadius = 15;
        bossView.layer.cornerRadius = 15;
        backButton.layer.cornerRadius = 10;
        
        recordButton.layer.cornerRadius = 0.5*recordButton.bounds.size.width;
        recordButton.clipsToBounds = true
        
        roomView.layer.cornerRadius = 15;
        
        pauseButton.layer.cornerRadius = 0.5*pauseButton.bounds.size.width;
        pauseButton.clipsToBounds = true
        
        if let nazwa = UserDefaults.standard.value(forKey: "room"){
        roomname.text = "\(nazwa)"
        }
        let docsDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var roomDirName = roomname.text! + "1"
        var index:Int = 1
        while(FileManager.default.fileExists(atPath: docsDir.appendingPathComponent(roomDirName).path)){
            index+=1
            roomDirName = roomname.text! + "\(index)"
        }
        do{
            try FileManager.default.createDirectory(at: docsDir.appendingPathComponent(roomDirName), withIntermediateDirectories: true, attributes: nil)
            UserDefaults.standard.setValue(roomDirName, forKey: "roomDirName")
        }catch{
            print("Nie moge stworzyc folderu: \(roomDirName)")
        }
        bossname.text = UserDefaults.standard.value(forKey: "name") as? String
        self.room?.setDelegate(delegate: self)
        print(self.room.debugDescription)
        //self.room?.getUsers()
        //var connectedUsers:[User]?
        //print(views.count)
        
        //Ustawienie przycisku pauzy nagrywania
        pauseButton.backgroundColor = UIColor.systemRed
        pauseButton.tintColor = UIColor.white
        pauseButton.isHidden = true
        if #available(iOS 13.0, *) {
            pauseButton.setImage(UIImage(systemName: "pause"), for: .normal)
        } else {
            // Fallback on earlier versions
        }
        //pauseButton.isUserInteractionEnabled = false
        pauseButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        mainView.addSubview(pauseButton)
    }
    //Akcja po przycisku pauzy
    @objc func buttonAction(sender: UIButton!){
        status2 = true
        statusLabel.text = "Recording is paused"
        if #available(iOS 13.0, *) {
            recordButton.setImage(UIImage(systemName: "play"), for: .normal)
        } else {
            // Fallback on earlier versions
        }
        self.room?.sendPauseRecording()
    }
    var pauseButton = UIButton(frame: CGRect(x: 100, y: 540, width: 50, height: 50))
    
    var status: Bool! = false
    var status2: Bool! = false
    
    @IBAction func sendStartRecording(_ sender: Any) {
        if(status2 == false){
            
        if(status == false){
            pauseButton.isHidden = false
            makeSessionDir()
            self.room?.sendStartRecording()
            self.room?.setFilesCount()
            print("recording started")
            
            statusLabel.text = "Recording"
            status = true
            if #available(iOS 13.0, *) {
                recordButton.setImage(UIImage(systemName: "stop"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
        }
        else
        {
            pauseButton.isHidden = true
            statusLabel.text = "Recording finished"
            if #available(iOS 13.0, *) {
                recordButton.setImage(UIImage(systemName: "mic.fill"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            
            
            self.room?.sendStopRecording()
            print("recording stopped")
            status = false
        }
        }
        else
        {
            //Co ma sie stac w chwili wznowienia po pauzie
            if #available(iOS 13.0, *) {
                recordButton.setImage(UIImage(systemName: "stop"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            statusLabel.text = "Recording"
            
            self.room?.sendStartRecording()
            //timer.fire()
            //timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownAction), userInfo: nil, repeats: true)
            //self.room?.send
            status2 = false
        }
        
    }
    
    @IBAction func exitRoom(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "menu")
        self.room?.leaveRoom()
        let avs = AVAudioSession.sharedInstance()
        do{
            try avs.setCategory(.playback, mode: .spokenAudio, options: .defaultToSpeaker)
        }catch{
            
        }
        //self.dismiss(animated: true, completion: nil)
        //secondVC.dismiss(animated: true, completion: nil)
        show(secondVC, sender: nil)
    }
    
    func makeSessionDir(){
        //let dirName = UserDefaults.value(forKey: "roomDirName")
        //let roomDir = FileManager.default.urls(for: .documentDirectory, in:  .userDomainMask)[0].appendingPathComponent("\(dirName ?? "")")
        let roomDir = FileManager.default.urls(for: .documentDirectory, in:  .userDomainMask)[0].appendingPathComponent(UserDefaults.standard.value(forKey:"roomDirName" )as! String)
        indexOfSession += 1
        let sessionDir = roomDir.appendingPathComponent("\(indexOfSession)")
        do{
            try FileManager.default.createDirectory(at: sessionDir, withIntermediateDirectories: true, attributes: nil)
            self.room?.setSessionDir(sessionDir: sessionDir)
        }catch{
            print("Nie moge utworzyc katalogu sesji")
        }
    }
}
