//
//  scrollViewAuto.swift
//  podcast
//
//  Created by Krzyś on 18/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit

class ScrollViewCreator {
    let mainView: UIView
    let scrollView: UIScrollView
    
    init(superView: UIView){
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        mainView = UIView()
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        superView.addSubview(scrollView)
        scrollView.addSubview(mainView)
        
        NSLayoutConstraint.activate([
            superView.leftAnchor.constraint(equalTo: mainView.leftAnchor),
            superView.rightAnchor.constraint(equalTo: mainView.rightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            scrollView.leftAnchor.constraint(equalTo: superView.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: superView.rightAnchor),
            scrollView.topAnchor.constraint(equalTo: superView.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: superView.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            mainView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
            mainView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
            mainView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            mainView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        ])
    }
    
    func addHorizontalConstrains(_ views: [UIView], _ horizontalPadding: CGFloat = 0.0){
        views.forEach { view in
            NSLayoutConstraint.activate([
                view.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: horizontalPadding),
                view.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: -horizontalPadding)
            ])
        }
    }
    
    func addVerticalConstrains(_ views: [UIView], _ verticalPadding: CGFloat = 0.0, _ edgeExtra: CGFloat = 0.0){
        guard !views.isEmpty else {
            return
        }
        if views.count == 1, let view = views.first{
            view.topAnchor.constraint(equalTo: mainView.topAnchor, constant: (verticalPadding + edgeExtra)).isActive = true
        } else {
            //First view to top of the page
            if let firstView = views.first {
                firstView.topAnchor.constraint(equalTo: mainView.topAnchor, constant: (verticalPadding + edgeExtra)).isActive = true
            }
            
            //Last view to bottom of the page
            if let lastView = views.last {
                lastView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -(verticalPadding + edgeExtra)).isActive = true
            }
            
            var temporaryViews = views
            var previousView = temporaryViews.first!
            
            temporaryViews.removeFirst()
            
            //Each of the remaining views to bottom of the previous
            for view in temporaryViews{
                view.topAnchor.constraint(equalTo: previousView.bottomAnchor, constant: verticalPadding).isActive = true
                previousView = view
            }
        }
    }
}
