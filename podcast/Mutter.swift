//
//  Mutter.swift
//  podcast
//
//  Created by Krzysztof on 18/05/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

protocol MutterDelegate:class {
    func didFinishWork()
}

public class Mutter{
    weak var delegate:MutterDelegate?
    
    
    private let fm = FileManager.default
    private var wordsArray = [String]()
    private var tsArray = [Double]()
    private var durationArray = [Double]()
    let group = DispatchGroup()
    
    //Saving original files URLs
    private var audioURL:URL!
    private var wordsURL:URL!
    private var timestampsURL:URL!
    private var transcriptionURL:URL!
    private var durationURL:URL!
    
    //Saving original files paths
    private let originPath:URL!
    private let originAudio:URL!
    private let originText:URL!
    private let originTimestamps:URL!
    private let originTranscription:URL!
    private let originDuration:URL!
    
    //Informations about first part
    private var outputPathFirst: URL!
    private var timerangeFirst: CMTimeRange!
    private var silenceStart: CMTime!
    private var indexFirst:Int = 0
    
    //Informations about second part
    private var outputPathSecond: URL!
    private var timerangeSecond: CMTimeRange!
    private var silenceStop: CMTime!
    private var indexSecond:Int = 0
    
    init(audio: URL, tsURL: URL, wURL: URL, durURL:URL!, trans: URL, duration: [Double], timestamps: [Double], words: [String]){
        print("Inicjalizacja muttera!!!")
        audioURL = audio
        timestampsURL = tsURL
        wordsURL = wURL
        transcriptionURL = trans
        durationURL = durURL
        
        tsArray = timestamps
        durationArray = duration
        wordsArray = words
        
        originPath = audioURL.deletingLastPathComponent().appendingPathComponent("ORIGIN", isDirectory: true)
        originAudio = originPath.appendingPathComponent(audioURL.deletingPathExtension().lastPathComponent).appendingPathExtension("ORIGINAL.m4a")
        originText = originPath.appendingPathComponent(audioURL.deletingPathExtension().lastPathComponent).appendingPathExtension("ORIGINAL.words.txt")
        originTimestamps = originPath.appendingPathComponent(audioURL.deletingPathExtension().lastPathComponent).appendingPathExtension("ORIGINAL.timestamps.txt")
        originTranscription = originPath.appendingPathComponent(audioURL.deletingPathExtension().lastPathComponent).appendingPathExtension("ORIGINAL.txt")
        originDuration = originPath.appendingPathComponent(audioURL.deletingPathExtension().lastPathComponent).appendingPathExtension("ORIGINAL.duration.txt")
    }
    
    private func copyFileToOrigin(urlFrom: URL, urlDestination: URL){
        if !fm.fileExists(atPath: urlDestination.path) {
            //Reading
            do {
                try fm.copyItem(at: urlFrom, to: urlDestination)
            } catch {
                print(error)
            }
        }
    }
    
    public func createOrigin(){
        if !fm.fileExists(atPath: originPath.path) {
            do {
                try fm.createDirectory(at: originPath, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("ERROR: \(error)")
            }
        }
        
        copyFileToOrigin(urlFrom: audioURL, urlDestination: originAudio)
        copyFileToOrigin(urlFrom: wordsURL, urlDestination: originText)
        copyFileToOrigin(urlFrom: timestampsURL, urlDestination: originTimestamps)
        copyFileToOrigin(urlFrom: transcriptionURL, urlDestination: originTranscription)
        copyFileToOrigin(urlFrom: durationURL, urlDestination: originDuration)
    }
    
    private func mergeTracks(){
        print("MERGE")
        let composition = AVMutableComposition()
        let mixTracks = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: CMPersistentTrackID())
        
        //First part
        var asset1: AVURLAsset!
        var tracks1: [AVAssetTrack]
        var assetTrack1: AVAssetTrack!
        var trFirst:CMTimeRange!
        
        group.enter()
        asset1 = AVURLAsset(url: outputPathFirst)
        trFirst = CMTimeRangeFromTimeToTime(start: CMTime.zero, end: asset1.duration)
        group.leave()
        let _ = group.wait(timeout: .now() + 0.5)
        tracks1 = asset1.tracks(withMediaType: .audio)
        assetTrack1 = tracks1[0]
        do {
            try mixTracks?.insertTimeRange(trFirst, of: assetTrack1, at: CMTime.zero)
        } catch {
            print("FIRST: \(error)")
        }
        
        //Second part
        //Variables
        var asset2: AVURLAsset!
        var tracks2: [AVAssetTrack]
        var assetTrack2: AVAssetTrack!
        var trSecond:CMTimeRange!
        
        //Setting
        group.enter()
        asset2 = AVURLAsset(url: outputPathSecond)
        trSecond = CMTimeRangeFromTimeToTime(start: CMTime.zero, end: asset2.duration)
        group.leave()
        let _ = group.wait(timeout: .now() + 0.5)
        tracks2 = asset2.tracks(withMediaType: .audio)
        assetTrack2 = tracks2[0]
        print(trSecond)
        
        //Adding to composition
        do {
            try mixTracks?.insertTimeRange(trSecond, of: assetTrack2, at: asset1.duration)
            
        } catch {
            print("SECOND: \(error)")
        }
        
        //Silent part
        let silence = CMTimeRangeFromTimeToTime(start: silenceStart, end: silenceStop)
        mixTracks?.insertEmptyTimeRange(silence)
        print(silence.duration, silence.start, silence.end, asset1.duration)
        
        //Delete previous version
        if fm.fileExists(atPath: audioURL.path){
            do {
                try fm.removeItem(at: audioURL)
            } catch {
                print(error)
            }
        }
        
        //Merging
        group.enter()
        if let export = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A) {
            export.outputURL = audioURL
            export.outputFileType = .m4a
            export.exportAsynchronously(completionHandler: {
                if export.status == .completed {
                    print("Completed extract")
                } else if export.status == .failed {
                    print("Failed")
                }
                self.group.leave()
            })
        }
        let _ = group.wait(timeout: .now() + 1)
        print("Completed extracting new audio")
        
        group.enter()
        //Update arrays
        if indexFirst == indexSecond {
            print("Usuwam: \(wordsArray[indexFirst]) \(tsArray[indexFirst]) \(durationArray[indexFirst])")
            wordsArray.remove(at: indexFirst)
            tsArray.remove(at: indexFirst)
            durationArray.remove(at: indexFirst)
        } else if indexFirst != indexSecond{
            for i in (indexFirst...indexSecond) {
                print("\(i): \(wordsArray[indexFirst]) \(tsArray[indexFirst]) \(durationArray[indexFirst])")
                wordsArray.remove(at: indexFirst)
                tsArray.remove(at: indexFirst)
                durationArray.remove(at: indexFirst)
            }
        }
        group.leave()
        group.notify(queue: .main) {
            print("Completed updating files")
        }
        
        //Save updated arrays to files
        group.enter()
        updateFiles()
        group.leave()
        group.notify(queue: .main) {
            print("Completed resaving files")
        }
    }
    
    private func removeFileIfExists(url: URL){
        if fm.fileExists(atPath: url.path){
            do {
                try fm.removeItem(at: url)
            } catch {
                print(error)
            }
        }
    }
    
    private func updateFiles(){
        group.enter()
        do {
            try fm.removeItem(at: transcriptionURL)
            try fm.removeItem(at: wordsURL)
            try fm.removeItem(at: timestampsURL)
            try fm.removeItem(at: durationURL)
            
            try wordsArray.joined(separator: " ").write(to: transcriptionURL, atomically: true, encoding: .utf8)
            try wordsArray.joined(separator: "-").write(to: wordsURL, atomically: true, encoding: .utf8)
            
            var tsString = [String]()
            for (index, _) in tsArray.enumerated() {
                tsString.append(String(describing: tsArray[index]))
            }
            try tsString.joined(separator: "-").write(to: timestampsURL, atomically: true, encoding: .utf8)
            
            var durString = [String]()
            for (index, _) in durationArray.enumerated() {
                durString.append(String(describing: durationArray[index]))
            }
            try durString.joined(separator: "-").write(to: durationURL, atomically: true, encoding: .utf8)
        } catch {
            print(error)
        }
        group.leave()
        let _ = group.wait(timeout: .now() + 0.5)
    }
    
    public func muteTracks(firstStart:Double, secondStart:Double, firstEnd:Double, secondEnd:Double, firstIndex:Int, secondIndex:Int){
        //Create parts
        group.enter()
        createOrigin()
        createAudioPart(extensionString: "p1.m4a", startValue: firstStart, endValue: firstEnd, number: 1, index: firstIndex)
        createAudioPart(extensionString: "p2.m4a", startValue: secondStart, endValue: secondEnd, number: 2, index: secondIndex)
        group.leave()
        let _ = group.wait(timeout: .now())
        print("Saved")
        
        group.enter()
        mergeTracks()
        group.leave()
        let _ = group.wait(timeout: .now())
        
        group.notify(queue: .main) {
            self.deleteAudioPart(number: 1)
            self.deleteAudioPart(number: 2)
            print("Deleted")
        }
    }
    
    //Creating parts of audio
    public func createAudioPart(extensionString: String, startValue: Double, endValue: Double, number: Int, index: Int){
        let asset = AVAsset(url: audioURL)
        let start = CMTime(seconds: startValue, preferredTimescale: 1000)
        let end = CMTime(seconds: endValue, preferredTimescale: 1000)
        let timeRange = CMTimeRangeFromTimeToTime(start: start, end: end)
        let outputPath = originPath.appendingPathComponent(audioURL.deletingPathExtension().lastPathComponent).appendingPathExtension(extensionString)
        
        if fm.fileExists(atPath: outputPath.path){
            do {
                try fm.removeItem(at: outputPath)
                print("DELETED \(number)")
            } catch {
                print(error)
            }
        }
        
        if number == 1 {
            outputPathFirst = outputPath
            timerangeFirst = timeRange
            silenceStart = end
            indexFirst = index
        } else if number == 2 {
            outputPathSecond = outputPath
            timerangeSecond = timeRange
            silenceStop = start
            indexSecond = index
        }
        guard let export = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else { return }
        export.outputURL = outputPath
        export.outputFileType = .m4a
        export.timeRange = timeRange
        
        group.enter()
        export.exportAsynchronously(completionHandler: {
            if export.status == .completed {
                print("Completed saving")
            } else if export.status == .failed {
                print("Failed saving \(number)")
            }
            self.group.leave()
        })
        let _ = group.wait(timeout: .now() + 0.5)
    }
    
    //Clearing temporary files
    private func deleteAudioPart(number: Int){
        var deletePath = originPath
        if number == 1 {
            deletePath = outputPathFirst
        } else if number == 2 {
            deletePath = outputPathSecond
        }
        
        do {
            try fm.removeItem(at: deletePath!)
        } catch {
            print(error)
        }
        print("Completed deleting")
    }
    
    public func revertChanges(){
        group.enter()
        copyAndReplace(at: audioURL, with: originAudio)
        copyAndReplace(at: wordsURL, with: originText)
        copyAndReplace(at: transcriptionURL, with: originTranscription)
        copyAndReplace(at: timestampsURL, with: originTimestamps)
        copyAndReplace(at: durationURL, with: originDuration)
        
        do {
            //Reading timestamps for time values
            let tsString = try String(contentsOf: originTimestamps)
            let str1 = tsString.split(separator: "-").map{ String($0) }
            str1.forEach({ (number) in
                tsArray.append((number as NSString).doubleValue)
            })
            
            //Reading words for highliting
            let wString = try String(contentsOf: originText)
            let str3 = wString.split(separator: " ").map{ String($0) }
            str3.forEach { (word) in
                wordsArray.append(word)
            }
        } catch {
            print(error)
        }
        
        group.leave()
        let _ = group.wait(timeout: .now())
        print("Finished replacing all files")
    }
    
    private func copyAndReplace(at: URL, with: URL){
        let copyName = originPath.appendingPathComponent("COPY")

        do {
            try fm.copyItem(at: with, to: copyName)
            let _ = try fm.replaceItemAt(at, withItemAt: copyName)
        } catch {
            print(error)
        }
    }
}
