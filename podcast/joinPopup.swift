//
//  joinPopup.swift
//  podcast
//
//  Created by Karol on 19/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit

class joinPopup: UIViewController,JoinCreateDelegate,UITextFieldDelegate{
    @IBOutlet weak var mybutton1: UIButton!
    var room:Room?
    var tmpName :UITextField = UITextField()
    @IBOutlet weak var nameOfRoom: UITextField!
    
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var downView: UIView!
    
    
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is JoinRecordRoomController{
            let vc = segue.destination as! JoinRecordRoomController
            vc.room = self.room
        }
        
    }
    @IBOutlet weak var mubutton2: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationItem.backBarButtonItem?.isEnabled = false;
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false;
        
        self.nameOfRoom.delegate = self
        
        mubutton2.layer.cornerRadius = 10;
        mybutton1.layer.cornerRadius = 10;
        upView.layer.cornerRadius = 15;
        downView.layer.cornerRadius = 15;
 
    }
    @IBAction func join(_ sender: Any) {
        //Zapisanie nazwy pokoju
        UserDefaults.standard.set(nameOfRoom.text, forKey: "room")
        //Zakomentowane do testowania
        self.room = Room()
        self.room?.setJoinCreateDelegate(delegate: self)
        self.room?.joinRoom(roomID: nameOfRoom.text)
         
    }
    func joinWithTmpName(name :String)
    {
        self.room?.setUsername(name: name)
        self.room?.joinRoom(roomID: nameOfRoom.text)
    }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func signal(joinRoomStatus success: Bool, withComunnicate comunicate: String) {
        if(success)
        {
            //przejscie do nastepnego ekranu
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let secondVC = storyboard.instantiateViewController(withIdentifier: "clientRoom") as! JoinRecordRoomController
            secondVC.room = self.room
           show(secondVC, sender: nil)
            //self.performSegue(withIdentifier: "joinToRoom", sender: self)
        }else{
            let alert = UIAlertController(title: title, message: comunicate, preferredStyle: .alert)
            if(comunicate == "Try a different name"){
                self.tmpName.text =  nil
                alert.addTextField(configurationHandler: {(tmp:UITextField!)->Void in tmp.placeholder = "Temporary username"
                })
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {
                    [weak alert](_)in
                let textField = alert?.textFields![0]
                self.joinWithTmpName(name: textField!.text!)
                
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            }else{
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            }
            alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
            alert.setTintColor(newColor: .white)
            alert.setMessageColor(newColor: .systemRed)
            present(alert, animated: true, completion: nil)
        }
    }
}

