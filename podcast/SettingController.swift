//
//  SettingController.swift
//  podcast
//
//  Created by Karol on 15/05/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit

class SettingsController:UIViewController,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var languageView: UIView!
    override func viewDidLoad() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        nameView.layer.cornerRadius = 15;
        languageView.layer.cornerRadius = 15;
    }
    @IBAction func changeName(_ sender: Any) {
    }
    @IBAction func changeLanguage(_ sender: Any) {
    }
}
