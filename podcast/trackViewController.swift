//
//  trackViewController.swift
//  podcast
//
//  Created by Krzyś on 18/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit
import FDWaveformView
import AVFoundation
import MultiSlider

class trackViewController: UIViewController{
    //Recived track informtion from previous view
    var reciveTrack: TrackInfo!
    
    //Main view
    var mainView: UIView!
    var scrollLayout: ScrollViewCreator!
    var viewsArray = [UIView]()
    
    //Used in playing audio
    var trackUpadte: CADisplayLink!
    var audioPlayer: AVAudioPlayer?
    
    //Radio buttons declaration
    let start = UIButton()
    let end = UIButton()
    
    //Sliders for cut and mute
    var doubleSliderCut = MultiSlider()
    var doubleSliderMute = MultiSlider()
    
    //MARK:============ MUTE ==================
    private var muteStartingTime:Double = 0.0
    private var muteEndingTime:Double = 0.0
    private let eRight = UIButton()
    private let eLeft = UIButton()
    private var oneWordBoolean = true
    private var selectedFirst = 0
    private var selectedLast = 0
    private let firstIndex = UILabel()
    private let lastIndex = UILabel()
    private let firstPrint = UILabel()
    private let lastPrint = UILabel()
    private var mutter:Mutter!
    
    //For saving parts in mutting
    var pathTMP:URL!
    
    //Waveform
    var waveForm =  FDWaveformView()
    
    //Error in muting occured
    let error = UILabel()
    
    //Progress variables
    var timeLabel = UILabel()
    var playButton = UIButton()
    var resetTrackTime = UIButton()
    var progressBar = MultiSlider()

    //Used in debugging waveform
    fileprivate var startRendering = Date()
    fileprivate var endRendering = Date()
    fileprivate var startLoading = Date()
    fileprivate var endLoading = Date()
    fileprivate var profileResult = ""
    
    //For playing
    var timer: Timer!
    
    //Coloring words in transcription
    var durArray = [Double]()
    var endStampsArray = [Double]()
    var tsArray = [Double]()
    var wordsArray = [String]()
    var coloredIndex = 0
    var lastColoredRange = 0
    var letterCount = [Int]()
    var attributedString:NSMutableAttributedString!
    let transcription = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollLayout = ScrollViewCreator(superView: self.view)
        mainView = scrollLayout.mainView
        mainView.translatesAutoresizingMaskIntoConstraints = false
        pathTMP = self.reciveTrack.urlAudio.deletingLastPathComponent().appendingPathComponent("tmp")

        
        //Make UI
        makeUI()
    }
    
    func makeUI(){
        //===================================================================================
        //MARK:Init transcription parts
        
        //Init audio
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: reciveTrack.urlAudio)
            audioPlayer?.prepareToPlay()
        } catch {
            print("Error")
        }
        
        //Clear checked words
        coloredIndex = 0
        
        do {
            //Reading timestamps for time values
            let tsString = try String(contentsOf: reciveTrack.urlTimestamps)
            let str1 = tsString.split(separator: "-").map{ String($0) }
            str1.forEach({ (number) in
                tsArray.append((number as NSString).doubleValue)
            })
            
            //Reading duration
            let durString = try String(contentsOf: reciveTrack.urlDuration)
            let str2 = durString.split(separator: "-").map{ String($0) }
            str2.forEach({ (number) in
                durArray.append((number as NSString).doubleValue)
            })
            
            //Setting "last" timestamps
            endStampsArray = durArray
            for (index, time) in tsArray.enumerated() {
                endStampsArray[index] += time
            }
            
            //Reading words for highliting
            let wString = try String(contentsOf: reciveTrack.urlWords)
            let str3 = wString.split(separator: "-").map{ String($0) }
            str3.forEach { (word) in
                wordsArray.append(word)
            }
            
            var previous = 0
            for i in wordsArray{
                letterCount.append(previous + i.count + 1)
                previous += i.count + 1
            }
        } catch {
            print("ERROR: \(error)")
        }
        
        //Initialize transcription text for later usage
        var tra: String = ""
        do {
            tra = try String(contentsOf: reciveTrack.urlText)
        } catch {
            print("ERROR: \(error)")
        }
        
        //Setting background color for transcription
        attributedString = NSMutableAttributedString(string: tra)
        attributedString.addAttribute(.backgroundColor, value: myGray2, range: NSMakeRange(0, attributedString.length))
        
        //Operator class for mutting
        mutter = Mutter(audio: reciveTrack.urlAudio,
                        tsURL:reciveTrack.urlTimestamps,
                        wURL: reciveTrack.urlWords,
                        durURL: reciveTrack.urlDuration,
                        trans: reciveTrack.urlText,
                        duration: durArray,
                        timestamps: tsArray,
                        words: wordsArray)
        //===================================================================================
        
        
        
        //===================================================================================
        //MARK:Track name
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = reciveTrack.name
        label.textColor = .white
        label.textAlignment = .center
        label.backgroundColor = myGray1
        label.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        //===================================================================================
        
        
        
        //===================================================================================
        //MARK:Logo
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "logo.png")
        imageView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        imageView.contentMode = .scaleAspectFill
        //===================================================================================
        
        
        
        //======================================
        //MARK:Graph view
        let graphView = makePlayAudioView()
        //======================================
        
        
        
        //======================================
        //MARK:Muting
        let muteView = makeMuteView()
        //======================================
        
        
        
        //======================================
        //MARK:BackButton
        let back = UIButton()
        back.translatesAutoresizingMaskIntoConstraints = false
        back.backgroundColor = myRedColor
        back.setTitle("BACK", for: .normal)
        back.addTarget(self, action: #selector(backButton), for: .touchUpInside)
        back.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        //======================================
        
        
        
        //======================================
        //MARK:Corners
        label.layer.cornerRadius = 10.0
        label.layer.masksToBounds = true
        back.layer.cornerRadius = 10.0
        back.layer.borderColor = myGray2.cgColor
        back.layer.borderWidth = 5.0
        //======================================
        
        
        
        //======================================
        //MARK:Adding
        viewsArray.append(imageView)
        viewsArray.append(label)
        viewsArray.append(graphView)
        viewsArray.append(muteView)
        viewsArray.append(back)
        //======================================
        
        
        
        //======================================
        //MARK:Layout everything
        for i in viewsArray{
            mainView.addSubview(i)
        }
        //======================================
        
        
        
        //======================================
        //MARK:Global constraintsConstrains
        scrollLayout.addHorizontalConstrains(viewsArray, 20.0)
        scrollLayout.addVerticalConstrains(viewsArray, 15.0, 20.0)
        //======================================
    }
    
    func makeMuteView() -> UIView{
        //===============================================================================================
        //MARK:Views
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        let view2 = UIView()
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.backgroundColor = myGray2
        let firstView = UIView()
        firstView.translatesAutoresizingMaskIntoConstraints = false
        firstView.backgroundColor = .black
        let lastView = UIView()
        lastView.translatesAutoresizingMaskIntoConstraints = false
        lastView.backgroundColor = .black
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Label
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "CUT OUT WORDS"
        label.textColor = .white
        label.textAlignment = .center
        label.backgroundColor = myGray1
        
        let option = UILabel()
        option.translatesAutoresizingMaskIntoConstraints = false
        option.text = "One word?"
        option.textAlignment = .left
        option.textColor = .white
        
        let askForFirst = UILabel()
        askForFirst.translatesAutoresizingMaskIntoConstraints = false
        askForFirst.text = "Starting word: "
        askForFirst.textAlignment = .left
        askForFirst.textColor = .white
        
        let askForLast = UILabel()
        askForLast.translatesAutoresizingMaskIntoConstraints = false
        askForLast.text = "Ending word: "
        askForLast.textAlignment = .left
        askForLast.textColor = .white
        
        firstPrint.translatesAutoresizingMaskIntoConstraints = false
        firstPrint.textAlignment = .center
        firstPrint.textColor = .white
        firstPrint.backgroundColor = .darkGray
        firstPrint.text = wordsArray[0]
        
        firstIndex.translatesAutoresizingMaskIntoConstraints = false
        firstIndex.text = "0"
        firstIndex.textAlignment = .center
        firstIndex.textColor = .white
        
        lastPrint.translatesAutoresizingMaskIntoConstraints = false
        lastPrint.textAlignment = .center
        lastPrint.textColor = .white
        lastPrint.backgroundColor = .darkGray
        lastPrint.text = wordsArray[0]
        
        lastIndex.translatesAutoresizingMaskIntoConstraints = false
        lastIndex.text = "0"
        lastIndex.textAlignment = .center
        lastIndex.textColor = .white
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Buttons
        let mute = UIButton()
        mute.translatesAutoresizingMaskIntoConstraints = false
        mute.backgroundColor = .darkGray
        mute.setTitle("PERFORM", for: .normal)
        mute.addTarget(self, action: #selector(muttingFunction), for: .touchUpInside)
        
        let revert = UIButton()
        revert.translatesAutoresizingMaskIntoConstraints = false
        revert.setTitle("REVERT", for: .normal)
        revert.backgroundColor = myGreenColor
        revert.addTarget(self, action: #selector(revertChanges), for: .touchUpInside)
        
        let checkbox = UIButton()
        checkbox.translatesAutoresizingMaskIntoConstraints = false
        checkbox.backgroundColor = myGreenColor
        checkbox.addTarget(self, action: #selector(changeCheckBox), for: .touchUpInside)
        checkbox.tag = 1
        
        let sRight = UIButton()
        sRight.translatesAutoresizingMaskIntoConstraints = false
        sRight.tag = 0
        sRight.addTarget(self, action: #selector(changeWord), for: .touchUpInside)
        sRight.backgroundColor = .lightGray
        
        let sLeft = UIButton()
        sLeft.translatesAutoresizingMaskIntoConstraints = false
        sLeft.tag = 1
        sLeft.addTarget(self, action: #selector(changeWord), for: .touchUpInside)
        sLeft.backgroundColor = .lightGray
        
        eRight.translatesAutoresizingMaskIntoConstraints = false
        eRight.tag = 2
        eRight.addTarget(self, action: #selector(changeWord), for: .touchUpInside)
        eRight.backgroundColor = .lightGray
        eRight.isHidden = true
        
        eLeft.translatesAutoresizingMaskIntoConstraints = false
        eLeft.tag = 3
        eLeft.addTarget(self, action: #selector(changeWord), for: .touchUpInside)
        eLeft.backgroundColor = .lightGray
        eLeft.isHidden = true
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Corners
        view2.layer.cornerRadius = 10.0
        view2.layer.masksToBounds = true
        label.layer.cornerRadius = 10.0
        label.layer.masksToBounds = true
        mute.layer.cornerRadius = 10.0
        mute.layer.masksToBounds = true
        firstPrint.layer.cornerRadius = 10.0
        firstPrint.layer.masksToBounds = true
        lastPrint.layer.cornerRadius = 10.0
        lastPrint.layer.masksToBounds = true
        checkbox.layer.cornerRadius = 10.0
        checkbox.layer.masksToBounds = true
        sRight.layer.cornerRadius = 5.0
        sLeft.layer.cornerRadius = 5.0
        eRight.layer.cornerRadius = 5.0
        eLeft.layer.cornerRadius = 5.0
        firstView.layer.cornerRadius = 10.0
        lastView.layer.cornerRadius = 10.0
        revert.layer.cornerRadius = 10.0
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Adding
        view2.addSubview(mute)
        view2.addSubview(checkbox)
        view2.addSubview(option)
        view2.addSubview(firstView)
        view2.addSubview(lastView)
        view2.addSubview(revert)
        
        firstView.addSubview(askForFirst)
        firstView.addSubview(firstPrint)
        firstView.addSubview(firstIndex)
        firstView.addSubview(sRight)
        firstView.addSubview(sLeft)
        
        lastView.addSubview(askForLast)
        lastView.addSubview(lastPrint)
        lastView.addSubview(eRight)
        lastView.addSubview(eLeft)
        lastView.addSubview(lastIndex)
        
        view.addSubview(label)
        view.addSubview(view2)
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Constraints
        NSLayoutConstraint.activate([
            //===========================================================================================
            //VIEWS
            view.heightAnchor.constraint(equalToConstant: 450.0),
            view2.widthAnchor.constraint(equalTo: view.widthAnchor),
            view2.topAnchor.constraint(equalTo: label.bottomAnchor),
            view2.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            firstView.topAnchor.constraint(equalTo: checkbox.bottomAnchor, constant: 10.0),
            firstView.leftAnchor.constraint(equalTo: view2.leftAnchor, constant: 5.0),
            firstView.rightAnchor.constraint(equalTo: view2.rightAnchor, constant: -10.0),
            firstView.heightAnchor.constraint(equalToConstant: 80.0),
            
            lastView.topAnchor.constraint(equalTo: firstView.bottomAnchor, constant: 10.0),
            lastView.leftAnchor.constraint(equalTo: view2.leftAnchor, constant: 5.0),
            lastView.rightAnchor.constraint(equalTo: view2.rightAnchor, constant: -10.0),
            lastView.heightAnchor.constraint(equalToConstant: 80.0),
            //===========================================================================================
            
            //===========================================================================================
            //SIZES
            label.heightAnchor.constraint(equalToConstant: 50.0),
            checkbox.widthAnchor.constraint(equalToConstant: 30),
            checkbox.heightAnchor.constraint(equalToConstant: 30.0),
            option.heightAnchor.constraint(equalToConstant: 30.0),
            
            askForFirst.heightAnchor.constraint(equalToConstant: 30.0),
            askForFirst.widthAnchor.constraint(equalToConstant: 110.0),
            askForLast.heightAnchor.constraint(equalToConstant: 30.0),
            askForLast.widthAnchor.constraint(equalToConstant: 110.0),
            
            sRight.widthAnchor.constraint(equalToConstant: 100.0),
            sLeft.widthAnchor.constraint(equalToConstant: 100.0),
            eRight.widthAnchor.constraint(equalToConstant: 100.0),
            eLeft.widthAnchor.constraint(equalToConstant: 100.0),
            
            sRight.heightAnchor.constraint(equalToConstant: 30.0),
            sLeft.heightAnchor.constraint(equalToConstant: 30.0),
            eRight.heightAnchor.constraint(equalToConstant: 30.0),
            eLeft.heightAnchor.constraint(equalToConstant: 30.0),
            
            firstPrint.heightAnchor.constraint(equalToConstant: 30.0),
            lastPrint.heightAnchor.constraint(equalToConstant: 30.0),
            
            firstIndex.widthAnchor.constraint(equalToConstant: 30.0),
            lastIndex.widthAnchor.constraint(equalToConstant: 30.0),
            
            mute.heightAnchor.constraint(equalToConstant: 80.0),
            //===========================================================================================
            
        
            
            //===========================================================================================
            //Title label
            label.topAnchor.constraint(equalTo: view.topAnchor),
            label.leftAnchor.constraint(equalTo: view.leftAnchor),
            label.rightAnchor.constraint(equalTo: view.rightAnchor),
            //===========================================================================================
            
            
            
            //===========================================================================================
            //Checking if we want cut only one word or multiple words
            checkbox.topAnchor.constraint(equalTo: view2.topAnchor, constant: 10.0),
            checkbox.leftAnchor.constraint(equalTo: view2.leftAnchor, constant: 10.0),
            
            option.topAnchor.constraint(equalTo: checkbox.topAnchor),
            option.leftAnchor.constraint(equalTo: checkbox.rightAnchor, constant: 10.0),
            option.rightAnchor.constraint(equalTo: view2.rightAnchor, constant: -10.0),
            //===========================================================================================
                
            
            
            //===========================================================================================
            //Asking for first word
            askForFirst.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 5.0),
            askForFirst.leftAnchor.constraint(equalTo: firstView.leftAnchor, constant: 5.0),
            
            firstPrint.centerYAnchor.constraint(equalTo: askForFirst.centerYAnchor),
            firstPrint.leftAnchor.constraint(equalTo: askForFirst.rightAnchor, constant: 5.0),
            
            firstIndex.topAnchor.constraint(equalTo: askForFirst.topAnchor),
            firstIndex.bottomAnchor.constraint(equalTo: askForFirst.bottomAnchor),
            firstIndex.leftAnchor.constraint(equalTo: firstPrint.rightAnchor, constant: 10.0),
            firstIndex.rightAnchor.constraint(equalTo: firstView.rightAnchor, constant: -5.0),
            
            sLeft.topAnchor.constraint(equalTo: firstPrint.bottomAnchor, constant: 5.0),
            sRight.topAnchor.constraint(equalTo: firstPrint.bottomAnchor, constant: 5.0),
            
            sLeft.rightAnchor.constraint(equalTo: firstView.centerXAnchor, constant: -15.0),
            sRight.leftAnchor.constraint(equalTo: firstView.centerXAnchor, constant: 15.0),
            //===========================================================================================
            
            
            
            //===========================================================================================
            //Asking for second word
            askForLast.topAnchor.constraint(equalTo: lastView.topAnchor, constant: 5.0),
            askForLast.leftAnchor.constraint(equalTo: lastView.leftAnchor, constant: 5.0),
            
            lastPrint.centerYAnchor.constraint(equalTo: askForLast.centerYAnchor),
            lastPrint.leftAnchor.constraint(equalTo: askForFirst.rightAnchor, constant: 5.0),
            
            lastIndex.topAnchor.constraint(equalTo: askForLast.topAnchor),
            lastIndex.bottomAnchor.constraint(equalTo: askForLast.bottomAnchor),
            lastIndex.leftAnchor.constraint(equalTo: lastPrint.rightAnchor, constant: 10.0),
            lastIndex.rightAnchor.constraint(equalTo: lastView.rightAnchor, constant: -5.0),
            
            eRight.topAnchor.constraint(equalTo: lastPrint.bottomAnchor, constant: 5.0),
            eLeft.topAnchor.constraint(equalTo: lastPrint.bottomAnchor, constant: 5.0),
            
            eLeft.rightAnchor.constraint(equalTo: view2.centerXAnchor, constant: -15.0),
            eRight.leftAnchor.constraint(equalTo: view2.centerXAnchor, constant: 15.0),
            //===========================================================================================
        
            
            
            //===========================================================================================
            //Confirm action
            mute.topAnchor.constraint(equalTo: lastView.bottomAnchor, constant: 10.0),
            mute.centerXAnchor.constraint(equalTo: view2.centerXAnchor),
            mute.leftAnchor.constraint(equalTo: view2.leftAnchor, constant: 10.0),
            mute.rightAnchor.constraint(equalTo: view2.rightAnchor, constant: -10.0),
            //===========================================================================================
            
            
            
            //===========================================================================================
            //Confirm action
            revert.topAnchor.constraint(equalTo: mute.bottomAnchor, constant: 10.0),
            revert.centerXAnchor.constraint(equalTo: view2.centerXAnchor),
            revert.leftAnchor.constraint(equalTo: view2.leftAnchor, constant: 10.0),
            revert.rightAnchor.constraint(equalTo: view2.rightAnchor, constant: -10.0),
            revert.bottomAnchor.constraint(equalTo: view2.bottomAnchor, constant: -10.0)
            //===========================================================================================
        ])
        //===============================================================================================
        
        return view
    }
    
    func makePlayAudioView() -> UIView{
        //===============================================================================================
        //MARK:Views
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let view2 = UIView()
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.backgroundColor = myGray2
        let upperGraph = UIView()
        upperGraph.translatesAutoresizingMaskIntoConstraints = false
        let progressView = UIView()
        progressView.translatesAutoresizingMaskIntoConstraints = false
        let transcriptionView = UIScrollView()
        transcriptionView.translatesAutoresizingMaskIntoConstraints = false
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Label
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "PLAY YOUR TRACK"
        label.textColor = .white
        label.textAlignment = .center
        label.backgroundColor = myGray1
        
        transcription.translatesAutoresizingMaskIntoConstraints = false
        transcription.backgroundColor = myGray2
        transcription.textAlignment = .left
        transcription.textColor = .white
        transcription.numberOfLines = 0
        transcription.attributedText = attributedString
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Buttons
        playButton.translatesAutoresizingMaskIntoConstraints = false
        playButton.setTitle("PLAY", for: .normal)
        playButton.tag = 0
        playButton.addTarget(self, action: #selector(playTrack), for: .touchUpInside)
        playButton.backgroundColor = .darkGray
        
        let addTime = UIButton()
        addTime.translatesAutoresizingMaskIntoConstraints = false
        addTime.setTitle("+5s", for: .normal)
        addTime.tag = 1
        addTime.addTarget(self, action: #selector(addTimeFunction), for: .touchUpInside)
        addTime.backgroundColor = .darkGray
        addTime.layer.cornerRadius = 20.0
        
        let removeTime = UIButton()
        removeTime.translatesAutoresizingMaskIntoConstraints = false
        removeTime.setTitle("-5s", for: .normal)
        removeTime.tag = 0
        removeTime.addTarget(self, action: #selector(addTimeFunction), for: .touchUpInside)
        removeTime.backgroundColor = .darkGray
        removeTime.layer.cornerRadius = 20.0

        resetTrackTime.translatesAutoresizingMaskIntoConstraints = false
        resetTrackTime.setTitle("RESET", for: .normal)
        resetTrackTime.addTarget(self, action: #selector(resetTime), for: .touchUpInside)
        resetTrackTime.backgroundColor = .darkGray
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Progress
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.textColor = .white
        timeLabel.textAlignment = .center
        timeLabel.text = "00:00.00"
        
        progressBar.tag = 00
        progressBar.thumbCount = 1
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.addTarget(self, action: #selector(setTime), for: .valueChanged)
        progressBar.addTarget(self, action: #selector(drop), for: .touchUpInside)
        progressBar.maximumValue = CGFloat(audioPlayer!.duration)
        progressBar.minimumValue = 0
        progressBar.orientation = .horizontal
        progressBar.tintColor = .darkGray
        
        let totalTimeDuration = UILabel()
        totalTimeDuration.translatesAutoresizingMaskIntoConstraints = false
        totalTimeDuration.textAlignment = .center
        totalTimeDuration.textColor = .white
        totalTimeDuration.text = calculateTime(time: CGFloat(audioPlayer!.duration))
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Graph
        waveForm.audioURL = reciveTrack.urlAudio
        waveForm.alpha = 0.0
        waveForm.delegate = self
        waveForm.wavesColor = .gray
        waveForm.progressColor = .gray
        waveForm.translatesAutoresizingMaskIntoConstraints = false
        waveForm.backgroundColor = myGray2
        waveForm.doesAllowScrubbing = false
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Corners
        view2.layer.cornerRadius = 10.0
        label.layer.cornerRadius = 10.0
        label.layer.masksToBounds = true
        transcriptionView.layer.borderColor = UIColor.darkGray.cgColor
        transcriptionView.layer.borderWidth = 5.0
        transcriptionView.layer.cornerRadius = 10.0
        playButton.layer.cornerRadius = 10.0
        playButton.layer.masksToBounds = true
        resetTrackTime.layer.cornerRadius = 10.0
        resetTrackTime.layer.masksToBounds = true
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Views adding
        
        //MAIN
        view.addSubview(label)
        view.addSubview(view2)

        view2.addSubview(upperGraph)
        view2.addSubview(progressView)
        view2.addSubview(waveForm)
        view2.addSubview(transcriptionView)

        //PROGRES TIME BAR
        progressView.addSubview(label)
        progressView.addSubview(timeLabel)
        progressView.addSubview(progressBar)
        progressView.addSubview(totalTimeDuration)

        //PLAYING AUDIO
        upperGraph.addSubview(playButton)
        upperGraph.addSubview(addTime)
        upperGraph.addSubview(removeTime)
        upperGraph.addSubview(resetTrackTime)
        
        transcriptionView.addSubview(transcription)
        //===============================================================================================
        
        
        
        //===============================================================================================
        //MARK:Constraints
        NSLayoutConstraint.activate([
            //===========================================================================================
            //VIEWS
            view.heightAnchor.constraint(equalToConstant: 710.0),
            upperGraph.heightAnchor.constraint(equalToConstant: 80.0),
            progressView.heightAnchor.constraint(equalTo: progressBar.heightAnchor, constant: 20.0),
            transcriptionView.heightAnchor.constraint(equalToConstant: 400.0),
            
            view2.topAnchor.constraint(equalTo: label.bottomAnchor),
            view2.leftAnchor.constraint(equalTo: view.leftAnchor),
            view2.rightAnchor.constraint(equalTo: view.rightAnchor),
            view2.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            upperGraph.topAnchor.constraint(equalTo: view2.topAnchor),
            upperGraph.leftAnchor.constraint(equalTo: view.leftAnchor),
            upperGraph.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            progressView.topAnchor.constraint(equalTo: upperGraph.bottomAnchor),
            progressView.leftAnchor.constraint(equalTo: view.leftAnchor),
            progressView.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            transcriptionView.topAnchor.constraint(equalTo: waveForm.bottomAnchor, constant: 10.0),
            transcriptionView.leftAnchor.constraint(equalTo: view2.leftAnchor, constant: 10.0),
            transcriptionView.rightAnchor.constraint(equalTo: view2.rightAnchor, constant: -10.0),
            //===========================================================================================
            
            
            
            //===========================================================================================
            //SIZES
            label.heightAnchor.constraint(equalToConstant: 50.0),
            waveForm.heightAnchor.constraint(equalToConstant: 90.0),
            progressBar.heightAnchor.constraint(equalToConstant: 40.0),
            playButton.widthAnchor.constraint(equalToConstant: 70.0),
            resetTrackTime.widthAnchor.constraint(equalToConstant: 70.0),
            addTime.widthAnchor.constraint(equalToConstant: 40.0),
            addTime.heightAnchor.constraint(equalToConstant: 40.0),
            removeTime.heightAnchor.constraint(equalToConstant: 40.0),
            removeTime.widthAnchor.constraint(equalToConstant: 40.0),
            //===========================================================================================
            
            
            
            //===========================================================================================
            //OTHERS
            label.topAnchor.constraint(equalTo: view.topAnchor),
            label.leftAnchor.constraint(equalTo: view.leftAnchor),
            label.rightAnchor.constraint(equalTo: view.rightAnchor),
            
            playButton.topAnchor.constraint(equalTo: upperGraph.topAnchor, constant: 10.0),
            playButton.bottomAnchor.constraint(equalTo: upperGraph.bottomAnchor, constant: -10.0),
            playButton.leftAnchor.constraint(equalTo: upperGraph.leftAnchor, constant: 40.0),
            
            addTime.leftAnchor.constraint(equalTo: playButton.rightAnchor, constant: 10.0),
            addTime.centerYAnchor.constraint(equalTo: upperGraph.centerYAnchor),
            
            removeTime.rightAnchor.constraint(equalTo: resetTrackTime.leftAnchor, constant: -10.0),
            removeTime.centerYAnchor.constraint(equalTo: upperGraph.centerYAnchor),
            
            resetTrackTime.topAnchor.constraint(equalTo: upperGraph.topAnchor, constant: 10.0),
            resetTrackTime.bottomAnchor.constraint(equalTo: upperGraph.bottomAnchor, constant: -10.0),
            resetTrackTime.rightAnchor.constraint(equalTo: upperGraph.rightAnchor, constant: -40.0),
                
            progressBar.topAnchor.constraint(equalTo: upperGraph.bottomAnchor),
            progressBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10.0),
            progressBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10.0),
            
            waveForm.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 10.0),
            waveForm.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10.0),
            waveForm.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10.0),
            
            transcription.topAnchor.constraint(equalTo: transcriptionView.topAnchor, constant: 10.0),
            transcription.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20.0),
            transcription.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20.0),
            
            timeLabel.topAnchor.constraint(equalTo: progressBar.bottomAnchor),
            timeLabel.leftAnchor.constraint(equalTo: progressView.leftAnchor, constant: 10.0),
            
            totalTimeDuration.topAnchor.constraint(equalTo: progressBar.bottomAnchor),
            totalTimeDuration.rightAnchor.constraint(equalTo: progressView.rightAnchor, constant: -10.0)
            //===========================================================================================
        ])
        
        return view
    }
    
    //Check if transcription exists
    //YES - prnt it!
    //NO - Error
    func printTranscription(label: UILabel) -> String{
        var printer = "ABCD"
        
        if reciveTrack.urlText != nil {
            do {
                printer = try String(contentsOf: reciveTrack.urlText)
            } catch {
                printer = "ERROR: \(error)"
            }
        } else {
            printer = "===ERROR==="
            label.textAlignment = .center
            label.textColor = myRedColor
        }
        
        return printer
    }
    
    //Calculate input int time (e.g 123sec = 02:03)
    func calculateTime(time: CGFloat) -> String{
        var printer = ""
        var minutesToPrint = ""
        var secondsToPrint = ""
        
        let minutes = Int(time) / 60
        let seconds = Int(time) % 60
        
        let miliseconds = Int((time - CGFloat(Int(time))) * 100.0)
        let form = NumberFormatter()
        form.numberStyle = .decimal
        form.maximumFractionDigits = 0
        var miliString = form.string(from: miliseconds as NSNumber)
        
        if minutes <= 9 {
            minutesToPrint = "0\(minutes)"
        } else {
            minutesToPrint = String(minutes)
        }
        
        if seconds <= 9 {
            secondsToPrint = "0\(seconds)"
        } else {
            secondsToPrint = String(seconds)
        }
        
        if miliseconds <= 9 {
            miliString = "0\(miliseconds)"
        } else {
            miliString = String(miliseconds)
        }
        
        printer = "\(minutesToPrint):\(secondsToPrint).\(miliString!)"
        return printer
    }
    
    @objc func updateMuttingPoints(sender: MultiSlider){
        let index = sender.draggedThumbIndex
        muteStartingTime = Double(doubleSliderMute.value[0])
        muteEndingTime = Double(doubleSliderMute.value[1])

        doubleSliderMute.valueLabels[index].text = calculateTime(time: doubleSliderMute.value[index])
    }
    
    //Reset button functionality
    @objc func resetTime(sender: UIButton){
        var checker = 0
        
        //Check if player is playing
        if audioPlayer?.isPlaying == true{
            audioPlayer?.stop()
            checker = 1
        }
        
        //Change everything to 0
        timeLabel.text = "00:00.00"
        progressBar.value[0] = 0
        audioPlayer?.currentTime = 0
        coloredIndex = 0
        lastColoredRange = 0
        clearText()
        
        audioPlayer?.prepareToPlay()
        
        //Check if player was playing
        if checker == 1 {
            audioPlayer?.play()
        }
    }
    
    //Change time to value set by slider
    @objc func setTime(sender: MultiSlider){
        //Check if player is playing
        if audioPlayer?.isPlaying == true{
            audioPlayer?.stop()
            sender.tag = 10
        }
        
        //Change audio time
        audioPlayer?.currentTime = Double(progressBar.value[0])
        
        //If player was playing then start it
        if sender.tag == 11 {
            playTrack(sender: playButton)
            
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
            sender.tag = 00
        }

        //Set timelabel
        timeLabel.text = calculateTime(time: CGFloat(audioPlayer!.currentTime))
    }
    
    //Check if thumb was dropped and player was playing
    @objc func drop(sender: MultiSlider){
        if sender.tag == 10{
            sender.tag = 11
        }
    }
    
    //Play/Stop button functionality
    @objc func playTrack(sender: UIButton!){
        //Check if audio is playing
        //YES - stop it
        //No - play it
        if sender.tag == 0 {
            //Start audio
            audioPlayer?.volume = 10.0
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
            
            //Change button visuals
            sender.setTitle("STOP", for: .normal)
            sender.backgroundColor = myRedColor
            sender.tag = 1
            
            //Start and configure timer
            timer  = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (Timer) in
                self.updateContents()
                
                if self.audioPlayer!.isPlaying == false {
                    self.updateContents()
                    self.coloredIndex = 0
                    self.lastColoredRange = 0
                    
                    self.clearText()
                    
                    self.playTrack(sender: self.playButton)
                }
            }
        } else {
            //Stop audio
            audioPlayer?.stop()
            
            //Change button visuals
            sender.setTitle("PLAY", for: .normal)
            sender.backgroundColor = .darkGray
            sender.tag = 0
            
            timer.invalidate()
        }
    }
    
    //For timer
    //If running - increase time 1sec
    @objc func updateContents(){
        timeLabel.text = calculateTime(time: CGFloat(audioPlayer!.currentTime))
        progressBar.value[0] = CGFloat(audioPlayer!.currentTime)
        colorText()
    }
    
    //Color part of transcription
    func colorText(){
        let currTime = audioPlayer!.currentTime
        if coloredIndex < (tsArray.count) {
            if currTime > tsArray[coloredIndex] && currTime < endStampsArray[coloredIndex] {
                attributedString.addAttribute(.backgroundColor, value: UIColor.lightGray, range: NSMakeRange(letterCount[coloredIndex] - wordsArray[coloredIndex].count - 1, wordsArray[coloredIndex].count))
            } else if currTime > endStampsArray[coloredIndex] {
                attributedString.addAttribute(.backgroundColor, value: myGray2, range: NSMakeRange(0, attributedString.length))
                coloredIndex += 1
            }
            transcription.attributedText = attributedString
        }
    }
    
    func clearText(){
        attributedString.addAttribute(.backgroundColor, value: myGray2, range: NSMakeRange(0, attributedString.length))
        transcription.attributedText = attributedString
    }
    
    //+/-sec button functionality
    @objc func addTimeFunction(_ sender: UIButton!){
        if playButton.tag == 1 {
            audioPlayer?.stop()
        }
        
        clearText()
        
        if(sender.tag == 1){
            audioPlayer?.currentTime += 5
            progressBar.value[0] += 5
        } else {
            audioPlayer?.currentTime -= 5
            progressBar.value[0] -= 5
            
            coloredIndex = 0
            tsArray.forEach { ts in
                if audioPlayer!.currentTime > ts {
                    coloredIndex = tsArray.index(of: ts)!
                }
            }
        }
        timeLabel.text = calculateTime(time: CGFloat(audioPlayer!.currentTime))
        
        if playButton.tag == 1 {
            colorText()
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        }
    }
    
    @objc func changeWord(sender: UIButton){
        if sender.tag == 0{
            selectedFirst += 1
            if selectedFirst > wordsArray.count - 1 {
                selectedFirst = wordsArray.count - 1
            }
            if oneWordBoolean == false && selectedFirst > selectedLast {
                selectedFirst = selectedLast
            }
        } else if sender.tag == 1{
            selectedFirst -= 1
            if selectedFirst < 0 {
                selectedFirst = 0
            }
        } else if sender.tag == 2 {
            selectedLast += 1
            if selectedLast > wordsArray.count - 1{
                selectedLast = wordsArray.count - 1
            }
        } else if sender.tag == 3 {
            selectedLast -= 1
            if selectedLast < 0 {
                selectedLast = 0
            }
            if oneWordBoolean == false && selectedFirst > selectedLast {
                selectedLast = selectedFirst
            }
        }
        if oneWordBoolean == true {
            selectedLast = selectedFirst
        }
        
        firstIndex.text = String(describing: selectedFirst)
        firstPrint.text = wordsArray[selectedFirst]
        
        lastIndex.text = String(describing: selectedLast)
        lastPrint.text = wordsArray[selectedLast]
        
    }
    
    @objc func changeCheckBox(sender: UIButton){
        if sender.tag == 0{
            sender.tag = 1
            sender.backgroundColor = myGreenColor
            eRight.isHidden = true
            eLeft.isHidden = true
            oneWordBoolean = true
            
            lastIndex.text = String(describing: selectedFirst)
            lastPrint.text = wordsArray[selectedFirst]
            selectedLast = selectedFirst
        } else {
            sender.tag = 0
            sender.backgroundColor = .darkGray
            eRight.isHidden = false
            eLeft.isHidden = false
            oneWordBoolean = false
        }
    }
    
    @objc func muttingFunction(){
        print(selectedFirst, selectedLast)
        let group = DispatchGroup()
        group.enter()
        mutter.muteTracks(firstStart: 0.0,
                          secondStart: endStampsArray[selectedLast],
                          firstEnd: tsArray[selectedFirst],
                          secondEnd: audioPlayer!.duration,
                          firstIndex: selectedFirst,
                          secondIndex: selectedLast)
        group.leave()
        group.notify(queue: .main) {
            self.refreshView()
        }
    }
    
    @objc func revertChanges(){
        mutter.createOrigin()
        mutter.revertChanges()
        refreshView()
    }
    
    public func refreshView(){
        deleteAllConstraints()
        for i in viewsArray {
            i.removeFromSuperview()
        }
        viewsArray = []
        wordsArray = []
        tsArray = []
        durArray = []
        letterCount = []
        audioPlayer = nil
        selectedFirst = 0
        selectedLast = 0
        makeUI()
        self.view.layoutSubviews()
    }
    
    private func deleteAllConstraints(){
        viewsArray.forEach { (view) in
            view.constraints.forEach { (constraint) in
                constraint.isActive = false
            }
        }
    }
    
    @objc func backButton(){
        dismiss(animated: true, completion: nil)
    }
}

//For waveform troubleshooting
extension trackViewController: FDWaveformViewDelegate{
    func waveformViewWillLoad(_ waveformView: FDWaveformView) {
        startLoading = Date()
    }

    func waveformViewDidLoad(_ waveformView: FDWaveformView) {
        endLoading = Date()
        NSLog("FDWaveformView loading done, took %0.3f seconds", endLoading.timeIntervalSince(startLoading))
        profileResult.append(String(format: " load %0.3f ", endLoading.timeIntervalSince(startLoading)))
    }

    func waveformViewWillRender(_ waveformView: FDWaveformView) {
        startRendering = Date()
    }

    func waveformViewDidRender(_ waveformView: FDWaveformView) {
        endRendering = Date()
        NSLog("FDWaveformView rendering done, took %0.3f seconds", endRendering.timeIntervalSince(startRendering))
        profileResult.append(String(format: " render %0.3f ", endRendering.timeIntervalSince(startRendering)))
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            waveformView.alpha = 1.0
        })
    }
}
