//
//  editorViewController.swift
//  podcast
//
//  Created by Krzyś on 17/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit
import AVFoundation

struct TrackInfo {
    var name: String
    var transcription: String
    var urlWave: URL!
    var urlPlayer: URL!
    var timeRange: CMTime!
    
    init(){
        name = ""
        transcription = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    }
}

class editorViewController: UIViewController{
    private var scrollLayout: ScrollViewCreator!
    private var labelTop = UILabel()
    private var createButton = UIButton()
    
    let buttonNames: [String] = ["Adam", "Kuba", "Zbyszek",
                                 "Jan", "Wojciech", "Adam",
                                 "Kuba", "Zbyszek", "Jan",
                                 "Wojciech", "Adam", "Kuba"]
    let waveformView = UIImageView()
    private var everyView: [UIView] = [UIView]()
    private var sendTrack = TrackInfo()
    private var currentTag = 0
    var trackPack = [TrackInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .darkGray
        
        scrollLayout = ScrollViewCreator(superView: self.view)
        let mainView = scrollLayout.mainView
        
        makeUI()
        
        for i in everyView{
            mainView.addSubview(i)
        }
        
        scrollLayout.addHorizontalConstrains(everyView, 20.0)
        scrollLayout.addVerticalConstrains(everyView, 10.0, 40.0)
    }
    
    func makeUI(){
        labelTop.text = "Choose your track"
        labelTop.numberOfLines = 0
        labelTop.textAlignment = .center
        labelTop.textColor = .white
        labelTop.translatesAutoresizingMaskIntoConstraints = false
        
        createButton.layer.borderWidth = 5.0
        createButton.layer.borderColor = UIColor.lightGray.cgColor
        createButton.layer.cornerRadius = 15.0
        createButton.setTitle("Create MP3 file", for: .normal)
        createButton.setTitleColor(.white, for: .normal)
        createButton.translatesAutoresizingMaskIntoConstraints = false
        createButton.backgroundColor = .gray
        createButton.tag = buttonNames.count
        createButton.addTarget(self, action: #selector(createTrack), for: .touchUpInside)
        createButton.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        
        everyView.append(labelTop)
        createButtonArray()
        everyView.append(createButton)
    }
    
    func createButtonArray(){
        /*for (index, name) in buttonNames.enumerated(){
            var tmpButton = TrackInfo()
            let url = Bundle.main.url(forResource: "bensound-summer", withExtension: "mp3")
            let urlAudio = URL(fileURLWithPath: Bundle.main.path(forResource: "bensound-summer.mp3", ofType: nil)!)
            
            let btn = UIButton(frame: CGRect.zero)
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.layer.cornerRadius = 15.0
            btn.setTitle(name, for: .normal)
            btn.setTitleColor(.white, for: .normal)
            btn.backgroundColor = .gray
            btn.tag = index
            btn.addTarget(self, action: #selector(chooseTrack), for: .touchUpInside)
            btn.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
            
            tmpButton.name = name
            tmpButton.urlWave = url
            tmpButton.urlPlayer = urlAudio
            
            trackPack.append(tmpButton)
            everyView.append(btn)
        }*/
    }
    
    func readDirectory(){}
    
    //sending segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if segue.identifier == "trackEditor"{
            let nextVC = segue.destination as! trackViewController
            nextVC.reciveTrack = sendTrack
        }
        if segue.identifier == "trackCreator"{
            let nextVC = segue.destination as! creationViewController
            nextVC.tracks = trackPack
        }
        
    }
    
    //Move to track editor
    @objc func chooseTrack(sender: UIButton!){
        sendTrack = trackPack[sender.tag]
        performSegue(withIdentifier: "trackEditor", sender: self)
    }
    
    //Export every track to one file
    @objc func createTrack(sender: UIButton!){
        performSegue(withIdentifier: "trackCreator", sender: self)
    }
}

