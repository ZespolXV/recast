//
//  creationViewController.swift
//  podcast
//
//  Created by Krzyś on 19/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit
import AVKit

class creationViewController: UIViewController{
    var tracks = [TrackInfo]()
    let fileName = "resultOfMerge.m4a"
    private var tracksAmount = UILabel()
    private var create = UIButton()
    private var amount = 0
    private var buttonsPacks: [UIView] = [UIView]()
    private var tmpButtons: [UIButton] = [UIButton]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .darkGray
        
        let scrollLayout: ScrollViewCreator!
        scrollLayout = ScrollViewCreator(superView: self.view)
        
        let mainView = scrollLayout.mainView
        
        makeUI()
        for i in buttonsPacks{
            mainView.addSubview(i)
        }
        
        scrollLayout.addVerticalConstrains(buttonsPacks, 10.0, 40.0)
        scrollLayout.addHorizontalConstrains(buttonsPacks, 20.0)
    }
    
    func makeUI(){
        let upper = UIStackView()
        upper.translatesAutoresizingMaskIntoConstraints = false
        upper.axis = .horizontal
        
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Choose all tracks to use"
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        
        tracksAmount.translatesAutoresizingMaskIntoConstraints = false
        tracksAmount.text = "Total: \(amount)"
        tracksAmount.textAlignment = .center
        tracksAmount.textColor = .white
        
        create.setTitle("Create", for: .normal)
        create.translatesAutoresizingMaskIntoConstraints = false
        create.addTarget(self, action: #selector(mergeTracks), for: .touchUpInside)

        let checkAll = UIButton()
        checkAll.setTitle("Check all", for: .normal)
        checkAll.translatesAutoresizingMaskIntoConstraints = false
        checkAll.addTarget(self, action: #selector(checkAllBoxes), for: .touchUpInside)
        checkAll.layer.cornerRadius = 25.0
        checkAll.layer.borderColor = UIColor.gray.cgColor
        checkAll.layer.borderWidth = 5.0
        checkAll.tag = 0
        
        upper.addSubview(checkAll)
        upper.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            upper.heightAnchor.constraint(equalToConstant: 50.0),
            checkAll.widthAnchor.constraint(equalToConstant: 125.0),
            
            checkAll.leftAnchor.constraint(equalTo: upper.leftAnchor),
            checkAll.topAnchor.constraint(equalTo: upper.topAnchor),
            checkAll.bottomAnchor.constraint(equalTo: upper.bottomAnchor),
            
            titleLabel.leftAnchor.constraint(equalTo: checkAll.rightAnchor, constant: 10.0),
            titleLabel.topAnchor.constraint(equalTo: upper.topAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: upper.bottomAnchor)
        ])
        
        buttonsPacks.append(upper)
        makeButtons()
        buttonsPacks.append(create)
        buttonsPacks.append(tracksAmount)
    }
    
    func makeButtons(){
        for (index, track) in tracks.enumerated() {
            let view = UIStackView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.axis = .horizontal
            view.backgroundColor = .red
            
            let checkBox = UIButton()
            checkBox.translatesAutoresizingMaskIntoConstraints = false
            checkBox.setTitle("N", for: .normal)
            checkBox.backgroundColor = .systemRed
            checkBox.layer.cornerRadius = 25.0
            checkBox.layer.borderColor = UIColor.gray.cgColor
            checkBox.layer.borderWidth = 5.0
            checkBox.addTarget(self, action: #selector(changeBoxContent), for: .touchUpInside)
            checkBox.isSelected = false
            checkBox.tag = index
            
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = track.name
            label.textColor = .white
            
            view.addSubview(checkBox)
            view.addSubview(label)
            
            NSLayoutConstraint.activate([
                //Sizes
                view.heightAnchor.constraint(equalToConstant: 50.0),
                checkBox.widthAnchor.constraint(equalToConstant: 50.0),
                
                //Sticking checkbox to the left
                checkBox.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 31.25),
                checkBox.topAnchor.constraint(equalTo: view.topAnchor),
                checkBox.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                
                //Sticking labels to checkboxes
                label.topAnchor.constraint(equalTo: view.topAnchor),
                label.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                label.leftAnchor.constraint(equalTo: checkBox.rightAnchor, constant: 10.0),
                label.rightAnchor.constraint(equalTo: view.rightAnchor)
            ])
            tmpButtons.append(checkBox)
            buttonsPacks.append(view)
        }
    }
    
    @objc func mergeTracks(){
        let composition = AVMutableComposition()
        var selectedTracks = [TrackInfo]()
        
        
        //Take info from checked buttons
        for i in tmpButtons{
            if i.isSelected == true{
                selectedTracks.append(tracks[i.tag])
            }
        }

        //Prepare tracks
        for i in selectedTracks{
            let compositionAudioTrack = composition.addMutableTrack(withMediaType: .audio,
                                                                    preferredTrackID: CMPersistentTrackID())
            let audio = AVURLAsset(url: i.urlPlayer)
            let track = audio.tracks(withMediaType: .audio)
            let audioAssetTrack = track[0]
            let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: audio.duration)

            //print(audio)
            
            do{
                try compositionAudioTrack?.insertTimeRange(timeRange, of: audioAssetTrack, at: CMTime.zero)
            }catch{
                print(error)
            }
        }
        
        //Make result file destination
        let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileDestinationURL = documentDirectoryURL.appendingPathComponent(fileName)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: fileDestinationURL.path){
            do{
                print("File exists...")
                try fileManager.removeItem(at: fileDestinationURL)
            } catch {
                print("A")
                print("Error: \(error)")
            }
        }
        
        //Merge
        print("Start merging...")
        let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)
        assetExport!.outputFileType = AVFileType.m4a
        assetExport?.outputURL = fileDestinationURL
        assetExport?.exportAsynchronously(completionHandler: {
            switch assetExport!.status{
            case .failed:
                print("failed: \(String(describing: assetExport?.error))")
            case .unknown:
                print("unknown: \(String(describing: assetExport?.error))")
            case .waiting:
                print("waiting: \(String(describing: assetExport?.error))")
            case .exporting:
                print("exporting: \(String(describing: assetExport?.error))")
            case .completed:
                print("completed: \(String(describing: assetExport?.error))")
            case .cancelled:
                print("cancelled: \(String(describing: assetExport?.error))")
            }
        })
        
    }
    
    @objc func checkAllBoxes(sender: UIButton!){
        if sender.isSelected == false{
            sender.isSelected = true
            sender.setTitle("Uncheck all", for: .normal)
            for i  in tmpButtons{
                i.setTitle("T", for: .normal)
                i.backgroundColor = .systemGreen
                i.isSelected = true
                amount = tmpButtons.count - 1
                tracksAmount.text = "Total: \(amount)"
            }
        } else {
            sender.isSelected = false
            sender.setTitle("Check all", for: .normal)
            for i  in tmpButtons{
                i.setTitle("N", for: .normal)
                i.backgroundColor = .systemRed
                i.isSelected = false
                amount = 0
                tracksAmount.text = "Total: \(amount)"
            }
        }
    }
    
    @objc func changeBoxContent(sender: UIButton!){
        if sender.isSelected == false{
            sender.isSelected = true
            sender.setTitle("T", for: .normal)
            sender.backgroundColor = .systemGreen
            amount += 1
            tracksAmount.text = "Total: \(amount)"
        } else {
            sender.isSelected = false
            sender.setTitle("N", for: .normal)
            sender.backgroundColor = .systemRed
            amount -= 1
            tracksAmount.text = "Total: \(amount)"
        }
    }
}
