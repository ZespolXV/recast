//
//  ViewController.swift
//  podcast
//
//  Created by Dawid Jenczewski on 28/01/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit
import WebRTC
class ViewController: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var welcome: UILabel!
    var room:Room?
    
    
    @IBOutlet weak var upView2: UIView!
    @IBOutlet weak var upView1: UIView!
    //override func viewDidLoad() {
    @IBOutlet weak var downn: UIView!
    @IBOutlet weak var upButton: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewDidLoad()
        
        if let imie = UserDefaults.standard.string(forKey: "language"){
            print(imie)

        }
                // Obcinanie rogow
        downView.layer.cornerRadius = 15;
        downView2.layer.cornerRadius = 15;
        downButton.layer.cornerRadius = 10;
        upView1.layer.cornerRadius = 15;
        upView2.layer.cornerRadius = 15;
        upButton.layer.cornerRadius = 10;
        downn.layer.cornerRadius = 15;
        self.navigationController?.navigationController?.interactivePopGestureRecognizer?.delegate = self
        //wstawienie imienia na poczatku
        if let imie = UserDefaults.standard.value(forKey: "name"){
            welcome.text = "Hello,  \(imie) !"
        }
        //self.navigationControllernavigationItem.backBarButtonItem?.isEnabled = false
        self.navigationController?.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    @IBAction func joinRoom(_ sender: UIButton) {
        //Zakomentowane do testowania
        //self.room = Room()
        //self.room?.joinRoom(roomID: roomName.text)
        
    }
    // dowiazanie twarde odpowiednio okno z ustawieniami, okno z Create Room na srodku, przycik Create Room roomSettingsView;
    /*@IBAction func disconnect(_ sender: UIButton) {
        //client?.disconnect()
    }*/
    @IBOutlet var roomSettingsView: UIView!
    @IBOutlet var createRoomView: UIView!
    @IBOutlet var createRoomButton: UIButton!
    //
    
    @IBOutlet weak var roomName: UITextField!
    
    @IBOutlet weak var downView: UIView!
    
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var downView2: UIView!
}

