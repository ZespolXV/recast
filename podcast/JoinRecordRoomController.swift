//
//  JoinRecordRoomController.swift
//  podcast
//
//  Created by Karol on 19/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class JoinRecordRoomController: UIViewController,RoomsSignalDelegates,UIGestureRecognizerDelegate{
    
    func signal(receiveAllFiles state: Bool){
        print("Host odebral wszystkie pliki")
        dismiss(animated: false, completion: nil)
    }
    func signal(TransState state: Bool) {
        print("zakonczenie transkrypcji")
        self.room?.sendFiles()
        print("COKOLWIEK")
        
        dismiss(animated: false, completion: nil)
        
        let alert = UIAlertController(title: "Alert", message: "RWait for the host to receive the files.", preferredStyle: .alert)
                alert.setTintColor(newColor: .white)
                alert.setTitleColor(newColor: .systemRed)
                alert.setMessageColor(newColor: .systemRed)
                alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))

                present(alert, animated: true, completion: nil)
        
    }
    
    
    func signal(recordChangeState state: String) {
        if(state == "Start"){
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownAction), userInfo: nil, repeats: true)
            statusLabel.text = "Recording in progress"
        }else if(state == "Stop"){
            timer.invalidate()
            countdown = 0
            statusLabel.text = "Recording finished"
            
            let alert = UIAlertController(title: "Alert", message: "Wait for transctiption to finish.", preferredStyle: .alert)
                        alert.setTintColor(newColor: .white)
                        alert.setTitleColor(newColor: .systemRed)
                        alert.setMessageColor(newColor: .systemRed)
                        alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
            
                    present(alert, animated: true, completion: nil)
            
        }else{
            timer.invalidate()
            statusLabel.text = "Recording is pause"
        }
    }
    
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    var views = [UIView]()
    var labels = [UILabel]()
    var images = [UIImageView]()
    var images2 = [UIImageView]()
    var UsersCounter: Int = 0
    var connectedUsers:[User]?
    
    
    
    @IBOutlet weak var labelRoom: UILabel!
    @IBOutlet weak var viewRoom: UIView!
    
    func signal(userLostConnection user: User) {
        if(user.name == szef){
            
            let alert = UIAlertController(title: "Connection lost", message: "Your host leave session. Room is closed.", preferredStyle: .alert)
            alert.setTitleColor(newColor: .systemRed)//#colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1))
            alert.setMessageColor(newColor: .systemRed)//#colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1))
            alert.setTintColor(newColor: .white)
            alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {action in
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "menu")
                self.present(newViewController, animated: true, completion: nil)
                
                
            }))
                present(alert, animated: true, completion: nil)
            return;
            //print("WYSZEDL SZEEEEF!!!")
        }
        //print(user.name)
        var pomoc = 0
        var loserIndex = 0
        for x in labels{
            if(x.text == user.name){
                loserIndex = pomoc
            }
            pomoc = pomoc+1
        }
        views[loserIndex].layer.backgroundColor =  #colorLiteral(red: 0.3607843137, green: 0.3607843137, blue: 0.3607843137, alpha: 1)
        if #available(iOS 13.0, *) {
            images2[loserIndex].image = UIImage(systemName: "speaker.zzz")
        } else {
            // Fallback on earlier versions
        }
    }
    
    func lostConnection() {
        self.room?.lostConnection()
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Connection lost", message: "Your connection have been terminated. Try to reconnect to room.", preferredStyle: .alert)
            alert.setTintColor(newColor: .white)
            alert.setMessageColor(newColor: .systemRed)
            alert.setTitleColor(newColor: .systemRed)
            alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
                
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {action in
                
                /*let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "menu")
                self.present(newViewController, animated: true, completion: nil)*/
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondVC = storyboard.instantiateViewController(withIdentifier: "menu")
                self.show(secondVC, sender: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
       // return;
    }
    
    func signal(userReconnect user: User) {
        var pomoc = 0
        var loserIndex = 0
        for x in labels{
            if(x.text == user.name){
                loserIndex = pomoc
            }
            pomoc = pomoc+1
        }
        views[loserIndex].layer.backgroundColor =  #colorLiteral(red: 0.2722781003, green: 0.2722781003, blue: 0.2722781003, alpha: 1)
        if #available(iOS 13.0, *) {
            images2[loserIndex].image = UIImage(systemName: "speaker.3")
        } else {
            // Fallback on earlier versions
        }
    }
    var szef: String?
    @IBOutlet weak var infoView: UIView!
    func signal(UserLeave users :[User]) {
        
        var isthere = 0
        var licznik = 0
        for x in users{
            if(x.isHost == true){
                isthere += 1
            }
        }
      
        if(isthere == 0){
            let alert = UIAlertController(title: "Connection lost", message: "Host, closed connection.", preferredStyle: .alert)
            alert.setTitleColor(newColor: .systemRed)
            alert.setMessageColor(newColor: .systemRed)
            alert.setTintColor(newColor: .white)
            alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2671043311, green: 0.2697489284, blue: 0.2697489284, alpha: 1))
                
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {action in
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "menu")
                self.present(newViewController, animated: true, completion: nil)
                
                
            }))
                present(alert, animated: true, completion: nil)
            return;
        }
        for x in 0...UsersCounter-1{
            views[x].removeFromSuperview()
            labels[x].removeFromSuperview()
            images[x].removeFromSuperview()
            images2[x].removeFromSuperview()
        }
        views.removeAll()
        labels.removeAll()
        images.removeAll()
        images2.removeAll()
        connectedUsers = users
        UsersCounter = users.count
        
        var i = 0
        
        var pion = 16, poziom = 152, wysokosc = 55
               var pion2 = 150, poziom2 = 170, wysokosc2 = 24
               var pion3 = 30, poziom3 = 160, wysokosc3 = 19
               var pion4 = 300, poziom4 = 164, wysokosc4 = 19
               //var image: UIImage = UIImage(named: "person.circle.fill")!
               
               //Do testow na sztywno 3
               var pomoc = (connectedUsers!.count)
                print(pomoc)
               //let pomoc = 3
               for _ in 1...pomoc{
                   views.append(UIView())
                   labels.append(UILabel())
                   images.append(UIImageView())
                   images2.append(UIImageView())
               }
               
            //Znalezienie nazwy szefa
               for x in connectedUsers!{
                   if(x.isHost == true){
                       szef = x.name
                   }
               }
               
               //szef="testerSzef"
               //Wypelnienie pierwszego kafelka szefem
               views[0] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 55))
               views[0].layer.backgroundColor = #colorLiteral(red: 0.2722781003, green: 0.2722781003, blue: 0.2722781003, alpha: 1)
            views[0].layer.cornerRadius = 15;
        
               labels[0] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 20))
               labels[0].text = szef
            labels[0].textColor = .white
            labels[0].textAlignment = .left
               
               images[0] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
               images[0].tintColor = UIColor.red
        
                images2[0] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 30, height: 30))
               images2[i].tintColor = UIColor.white
               
        if #available(iOS 13.0, *) {
                   images2[0].image = UIImage(systemName: "speaker.3")
               } else {
                   // Fallback on earlier versions
               }
               if #available(iOS 13.0, *) {
                   images[0].image = UIImage(systemName: "person.circle.fill")
               } else {
                   // Fallback on earlier versions
               }
               
               mainView.addSubview(views[0])
               mainView.addSubview(labels[0])
               mainView.addSubview(images[0])
               mainView.addSubview(images2[0])
            
                       
               poziom = poziom+wysokosc+5
               poziom2 = poziom2+wysokosc+5
               poziom3 = poziom3+wysokosc+5
                poziom4 = poziom4+wysokosc+5
               
               //Wypelnienie lobby pozostalymi uzytkownikami
               while pomoc>i {
                   if(connectedUsers?[i].isHost == false){
                   views[i] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 55))
                   views[i].layer.backgroundColor =  #colorLiteral(red: 0.2722781003, green: 0.2722781003, blue: 0.2722781003, alpha: 1)
                    views[i].layer.cornerRadius = 15
                    
                    
                   labels[i] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 25))
                   labels[i].text = connectedUsers![i].name
                    labels[i].textAlignment = .left
                    labels[i].textColor = .white
                   
                   images[i] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
                    
                    images2[i] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 30, height:30))
                    
                    images[i].tintColor = UIColor.white
                    images2[i].tintColor = UIColor.white
                    if #available(iOS 13.0, *) {
                        images2[i].image = UIImage(systemName: "speaker.3")
                    } else {
                        // Fallback on earlier versions
                    }
                    if #available(iOS 13.0, *) {
                        images[i].image = UIImage(systemName: "person.circle.fill")
                    } else {
                        // Fallback on earlier versions
                    }
                    
                   
                   mainView.addSubview(views[i])
                   mainView.addSubview(labels[i])
                   mainView.addSubview(images[i])
                    mainView.addSubview(images2[i])
                   
                   
                   poziom = poziom+wysokosc+5
                   poziom2 = poziom2+wysokosc+5
                   poziom3 = poziom3+wysokosc+5
                    poziom4 = poziom4+wysokosc+5
                   //print(i)
               }
                   i = i+1
               }
    }    
    
    func signal(UsersLoaded users: [User]) {
        UsersCounter = users.count
        connectedUsers = users
        var i = 0
               var pion = 16, poziom = 152, wysokosc = 55
               var pion2 = 150, poziom2 = 170, wysokosc2 = 24
               var pion3 = 30, poziom3 = 160, wysokosc3 = 19
               var pion4 = 300, poziom4 = 164, wysokosc4 = 19
               //var image: UIImage = UIImage(named: "person.circle.fill")!
               
               //Do testow na sztywno 3
               var pomoc = (connectedUsers!.count)
                print(pomoc)
               //let pomoc = 3
               for _ in 1...pomoc{
                   views.append(UIView())
                   labels.append(UILabel())
                   images.append(UIImageView())
                   images2.append(UIImageView())
               }
               
            //Znalezienie nazwy szefa               
               for x in connectedUsers!{
                   if(x.isHost == true){
                       szef = x.name
                   }
               }
               
               //szef="testerSzef"
               //Wypelnienie pierwszego kafelka szefem
               views[0] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 55))
               views[0].layer.backgroundColor = #colorLiteral(red: 0.2722781003, green: 0.2722781003, blue: 0.2722781003, alpha: 1)
            views[0].layer.cornerRadius = 15;
        
               labels[0] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 20))
               labels[0].text = szef
            labels[0].textColor = .white
            labels[0].textAlignment = .left
               
               images[0] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
               images[0].tintColor = UIColor.red
        
                images2[0] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 30, height: 30))
               images2[i].tintColor = UIColor.white
               
        if #available(iOS 13.0, *) {
                   images2[0].image = UIImage(systemName: "speaker.3")
               } else {
                   // Fallback on earlier versions
               }
               if #available(iOS 13.0, *) {
                   images[0].image = UIImage(systemName: "person.circle.fill")
               } else {
                   // Fallback on earlier versions
               }
               
               mainView.addSubview(views[0])
               mainView.addSubview(labels[0])
               mainView.addSubview(images[0])
               mainView.addSubview(images2[0])
            
                       
               poziom = poziom+wysokosc+5
               poziom2 = poziom2+wysokosc+5
               poziom3 = poziom3+wysokosc+5
                poziom4 = poziom4+wysokosc+5
               
               //Wypelnienie lobby pozostalymi uzytkownikami
               while pomoc>i {
                   if(connectedUsers?[i].isHost == false){
                   views[i] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 55))
                    views[i].layer.backgroundColor = #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1)
                    views[i].layer.cornerRadius = 15
                    
                    
                   labels[i] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 25))
                   labels[i].text = connectedUsers![i].name
                    labels[i].textAlignment = .left
                    labels[i].textColor = .white
                   
                   images[i] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
                    
                    images2[i] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 30, height:30))
                    
                    images[i].tintColor = UIColor.white
                    images2[i].tintColor = UIColor.white
                    if #available(iOS 13.0, *) {
                        images2[i].image = UIImage(systemName: "speaker.3")
                    } else {
                        // Fallback on earlier versions
                    }
                    if #available(iOS 13.0, *) {
                        images[i].image = UIImage(systemName: "person.circle.fill")
                    } else {
                        // Fallback on earlier versions
                    }
                    
                   
                   mainView.addSubview(views[i])
                   mainView.addSubview(labels[i])
                   mainView.addSubview(images[i])
                    mainView.addSubview(images2[i])
                   
                   
                   poziom = poziom+wysokosc+5
                   poziom2 = poziom2+wysokosc+5
                   poziom3 = poziom3+wysokosc+5
                    poziom4 = poziom4+wysokosc+5
                   //print(i)
               }
                   i = i+1
               }
    }
    
    
    func signal(didJoin user: [User]) {
        UsersCounter = user.count
        connectedUsers = user
        var x:Int = 0
        while x < UsersCounter-1{
            views[x].removeFromSuperview()
            labels[x].removeFromSuperview()
            images[x].removeFromSuperview()
            images2[x].removeFromSuperview()
            x += 1
        }
        views.removeAll()
        labels.removeAll()
        images.removeAll()
        images2.removeAll()
        
        var i = 0
               var szef: String?
               var pion = 16, poziom = 152, wysokosc = 55
               var pion2 = 150, poziom2 = 170, wysokosc2 = 24
               var pion3 = 30, poziom3 = 160, wysokosc3 = 19
               var pion4 = 300, poziom4 = 164, wysokosc4 = 19
               //var image: UIImage = UIImage(named: "person.circle.fill")!
               
               //Do testow na sztywno 3
               var pomoc = (connectedUsers!.count)
                print(pomoc)
               //let pomoc = 3
               for _ in 1...pomoc{
                   views.append(UIView())
                   labels.append(UILabel())
                   images.append(UIImageView())
                   images2.append(UIImageView())
               }
               
            //Znalezienie nazwy szefa
               for x in connectedUsers!{
                   if(x.isHost == true){
                       szef = x.name
                   }
               }
               
               //szef="testerSzef"
               //Wypelnienie pierwszego kafelka szefem
               views[0] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 55))
               views[0].layer.backgroundColor = #colorLiteral(red: 0.2722781003, green: 0.2722781003, blue: 0.2722781003, alpha: 1)
            views[0].layer.cornerRadius = 15;
        
               labels[0] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 20))
               labels[0].text = szef
            labels[0].textColor = .white
            labels[0].textAlignment = .left
               
               images[0] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
               images[0].tintColor = UIColor.red
        
                images2[0] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 30, height: 30))
               images2[i].tintColor = UIColor.white
               
        if #available(iOS 13.0, *) {
                   images2[0].image = UIImage(systemName: "speaker.3")
               } else {
                   // Fallback on earlier versions
               }
               if #available(iOS 13.0, *) {
                   images[0].image = UIImage(systemName: "person.circle.fill")
               } else {
                   // Fallback on earlier versions
               }
               
               mainView.addSubview(views[0])
               mainView.addSubview(labels[0])
               mainView.addSubview(images[0])
               mainView.addSubview(images2[0])
            
                       
               poziom = poziom+wysokosc+5
               poziom2 = poziom2+wysokosc+5
               poziom3 = poziom3+wysokosc+5
                poziom4 = poziom4+wysokosc+5
               
               //Wypelnienie lobby pozostalymi uzytkownikami
               while pomoc>i {
                   if(connectedUsers?[i].isHost == false){
                   views[i] = UIView(frame: CGRect(x: pion, y: poziom, width: 343, height: 55))
                   views[i].layer.backgroundColor =  #colorLiteral(red: 0.2722781003, green: 0.2722781003, blue: 0.2722781003, alpha: 1)
                    views[i].layer.cornerRadius = 15
                    
                    
                   labels[i] = UILabel(frame: CGRect(x: pion2, y: poziom2, width: 172, height: 25))
                   labels[i].text = connectedUsers![i].name
                    labels[i].textAlignment = .left
                    labels[i].textColor = .white
                   
                   images[i] = UIImageView(frame: CGRect(x: pion3, y: poziom3, width: 40, height: 40))
                    
                    images2[i] = UIImageView(frame: CGRect(x: pion4, y: poziom4, width: 30, height:30))
                    
                    images[i].tintColor = UIColor.white
                    images2[i].tintColor = UIColor.white
                    if #available(iOS 13.0, *) {
                        images2[i].image = UIImage(systemName: "speaker.3")
                    } else {
                        // Fallback on earlier versions
                    }
                    if #available(iOS 13.0, *) {
                        images[i].image = UIImage(systemName: "person.circle.fill")
                    } else {
                        // Fallback on earlier versions
                    }
                    
                   
                   mainView.addSubview(views[i])
                   mainView.addSubview(labels[i])
                   mainView.addSubview(images[i])
                    mainView.addSubview(images2[i])
                   
                   
                   poziom = poziom+wysokosc+5
                   poziom2 = poziom2+wysokosc+5
                   poziom3 = poziom3+wysokosc+5
                    poziom4 = poziom4+wysokosc+5
                   //print(i)
               }
                   i = i+1
               }
    }
    
    var room:Room?
    @IBOutlet var mainView: UIView!
    //var connectedUsers:[User]?
    var timer: Timer!
    var countdown: Int = 0
    var hour: String = ""
    @objc func countDownAction(){
        countdown += 1
        if(countdown%60 < 10){
            hour = "\(countdown/60):0\(countdown%60)"
        }
        else
        {
            hour = "\(countdown/60):\(countdown%60)"
        }
        time.text = hour
    }
    var isRecording: Bool = false
    @IBOutlet weak var time: UILabel!
    override func viewDidLoad() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false;
        
        self.room?.setDelegate(delegate: self)
        self.room?.getUsers()
        infoView.layer.cornerRadius = 15;
        exitButton.layer.cornerRadius = 10;
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownAction), userInfo: nil, repeats: true)
        timer.invalidate()
        //nie martwic sie, w funkcji jest sleep na 1 sek, pozniej poprawie
       // navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        viewRoom.layer.cornerRadius = 15;
        labelRoom.layer.cornerRadius = 15;
        
        
        
        if let imie = UserDefaults.standard.value(forKey: "room"){
            labelRoom.text = imie as! String
        }
       
       
        
    }
    
    @IBAction func leaveRoom(_ sender: Any) {
        self.room?.leaveRoom()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "menu")
        let avs = AVAudioSession.sharedInstance()
        do{
            try avs.setCategory(.playback, mode: .spokenAudio, options: .defaultToSpeaker)
        }catch{
            
        }
        show(secondVC, sender: nil)
        
    }
    
}
