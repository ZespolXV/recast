//
//  DropDownButton.swift
//  podcast
//
//  Created by Krzyś on 22/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit

class DropDownButton: UIButton{
    var myButton: UIButton!
    var allViewsToAdd: UIView!
    var configurationBefore: NSLayoutConstraint!
    let mainLayout: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    init(_ width: CGFloat, _ height: CGFloat, _ everyView: UIView) {
        super.init(frame: .zero)
        myButton = UIButton()
        myButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        myButton.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.allViewsToAdd = everyView
        setupConstrains()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstrains(){
        configurationBefore.constant = 0.0
        NSLayoutConstraint.activate([
            allViewsToAdd.topAnchor.constraint(equalTo: myButton.bottomAnchor),
            allViewsToAdd.widthAnchor.constraint(equalToConstant: myButton.bounds.size.width),
            allViewsToAdd.heightAnchor.constraint(equalToConstant: configurationBefore.constant)
        ])
    }
    
    func changeHeight(_ number: CGFloat){
        configurationBefore.constant = number
    }
}
