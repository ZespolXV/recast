//
//  edytorMenuViewController.swift
//  podcast
//
//  Created by Krzysztof on 05/05/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import UIKit
import AVFoundation

struct TrackInfo {
    var name: String
    var part: String
    var transcription: String
    var urlAudio: URL!
    var urlText: URL!
    var urlTimestamps: URL!
    var urlWords: URL!
    var urlDuration:URL!
    var timeRange: CMTime!
    
    init(){
        part = ""
        name = ""
        transcription = "ABC"
    }
}

struct FolderInfo{
    var name: String
    var url: URL
    
    init(){
        name = ""
        
        let fm = FileManager.default
        let documentsURL = fm.urls(for: .documentDirectory, in: .userDomainMask)[0]
        url = documentsURL
    }
}

public var myGray1 = UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1)
public var myGray2 = UIColor(red: 68.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1)
public var myRedColor = UIColor(red: 222.0/255.0, green: 46.0/255.0, blue: 36.0/255.0, alpha: 1)
public var myGreenColor = UIColor(red: 89.0/255.0, green: 158.0/255.0, blue: 26.0/255.0, alpha: 1)

class edytorMenuViewController: UIViewController{
    private var scrollLayout: ScrollViewCreator!
    private var mainView = UIView()
    
    var buttonNames: [String] = [String]()
    var foldersInformation:[FolderInfo] = [FolderInfo]()
    
    var everyView: [UIView] = [UIView]()
    var sendFolder = FolderInfo()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .darkGray
        view.clearsContextBeforeDrawing = true
        
        scrollLayout = ScrollViewCreator(superView: self.view)
        mainView = scrollLayout.mainView
        readDirectory()
        makeUI()
    }
    
    func makeUI(){
        //===================================================================================
        //MARK:Logo
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFill
        
        //Add to main view
        everyView.append(imageView)
        //===================================================================================
        
        //===================================================================================
        //MARK:Making buttons for menu
        if buttonNames.count != 0 {
            createButtonArray()
        } else {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.numberOfLines = 0
            label.text = "NO RECORDINGS WERE FOUND!! \nNO RECORDINGS WERE FOUND!! \nNO RECORDINGS WERE FOUND!!"
            label.textAlignment = .center
            label.textColor = myRedColor
            
            view.addSubview(label)
            NSLayoutConstraint.activate([
                label.topAnchor.constraint(equalTo: view.topAnchor),
                label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10.0),
                label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10.0)
            ])
            everyView.append(view)
        }
        //===================================================================================
        
        for i in everyView{
            self.mainView.addSubview(i)
        }
        
        self.scrollLayout.addHorizontalConstrains(self.everyView, 20.0)
        self.scrollLayout.addVerticalConstrains(self.everyView, 15.0, 20.0)
    }

    
    func deleteAllConstraints(){
        everyView.forEach { (view) in
            view.constraints.forEach { (constraint) in
                constraint.isActive = false
            }
        }
    }
    
    func createButtonArray(){
        for (index, name) in buttonNames.enumerated(){
            //===================================================================================
            //MARK:Views
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            let view2 = UIView()
            view2.translatesAutoresizingMaskIntoConstraints = false
            view2.backgroundColor = myGray2
            //===================================================================================
            
            
            
            //===================================================================================
            //MARK:Name label
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.backgroundColor = myGray1
            label.text = name
            label.textAlignment = .center
            label.textColor = .white
            //===================================================================================
            
            
            
            //===================================================================================
            //MARK:Buttons
            let edit = UIButton(frame: CGRect.zero)
            edit.translatesAutoresizingMaskIntoConstraints = false
            edit.setTitle("Edit", for: .normal)
            edit.setTitleColor(.white, for: .normal)
            edit.backgroundColor = .gray
            edit.tag = index
            edit.addTarget(self, action: #selector(editorAction), for: .touchUpInside)
                
            let delete = UIButton()
            delete.translatesAutoresizingMaskIntoConstraints = false
            delete.setTitle("Delete", for: .normal)
            delete.setTitleColor(.white, for: .normal)
            delete.backgroundColor = myRedColor
            delete.tag = index
            delete.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
            //===================================================================================
            
            
            
            
            //===================================================================================
            //MARK:Adding
            view.addSubview(label)
            view.addSubview(view2)
            view2.addSubview(edit)
            view2.addSubview(delete)
            //===================================================================================
            
            
            
            //===================================================================================
            //MARK:Corners
            view.layer.cornerRadius = 10.0
            view.layer.masksToBounds = true
            view2.layer.cornerRadius = 10.0
            view2.layer.masksToBounds = true
            edit.layer.cornerRadius = 10.0
            edit.layer.masksToBounds = true
            delete.layer.cornerRadius = 10.0
            delete.layer.masksToBounds = true
            label.layer.cornerRadius = 10.0
            label.layer.masksToBounds = true
            //===================================================================================
            
            
            
            //===================================================================================
            //MARK:Constraints
            NSLayoutConstraint.activate([
                label.topAnchor.constraint(equalTo: view.topAnchor),
                label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                view2.topAnchor.constraint(equalTo: label.bottomAnchor),
                delete.topAnchor.constraint(equalTo: view2.topAnchor, constant: 10.0),
                edit.topAnchor.constraint(equalTo: view2.topAnchor, constant: 10.0),
                
                view.heightAnchor.constraint(equalToConstant: 110.0),
                view2.heightAnchor.constraint(lessThanOrEqualToConstant: 60.0),
                view2.widthAnchor.constraint(equalTo: view.widthAnchor),
                label.heightAnchor.constraint(equalToConstant: 50.0),
                label.widthAnchor.constraint(equalTo: view.widthAnchor),
                delete.heightAnchor.constraint(equalToConstant: 40.0),
                edit.heightAnchor.constraint(equalToConstant: 40.0),
                
                edit.centerXAnchor.constraint(equalTo: view2.centerXAnchor, constant: -50.0),
                delete.centerXAnchor.constraint(equalTo: view2.centerXAnchor, constant: 50.0),
                
                edit.widthAnchor.constraint(equalToConstant: 70.0),
                delete.widthAnchor.constraint(equalToConstant: 70.0)
            ])
            //===================================================================================
            
            //Add every view to mainview and mainview constraints
            everyView.append(view)
        }
    }
    
    func readDirectory(){
        let fm = FileManager.default
        let documentsURL = fm.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        //=========================================================================================================
        //MARK:Reading documents directory for directories
        do{
            let contents = try fm.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            var foldersFiles = contents.filter{ $0.pathExtension == "" }
            foldersFiles = foldersFiles.sorted{ $0.lastPathComponent < $1.lastPathComponent }
            
            for i in foldersFiles{
                var tmp = FolderInfo()
                
                tmp.name = i.lastPathComponent
                tmp.url = i
                foldersInformation.append(tmp)
                
                buttonNames.append(i.lastPathComponent)
            }
        } catch {
            print("Error \(error)")
        }
        //=========================================================================================================
    }
    
    @objc func deleteAction(sender: UIButton){
        let alert = UIAlertController(title: "Delete file", message: "Are you sure?", preferredStyle: .alert)
        
        alert.setBackgroundColor(newColor: myGray2)
        alert.setTitleColor(newColor: .white)
        alert.setMessageColor(newColor: .white)
        alert.setTintColor(newColor: .white)
        
        //MARK:Yes statement
        let agree = UIAlertAction(title: "YES", style: .destructive, handler: { (_) in
            //=====================================================================================================
            //MARK:Delete folder path
            let fm = FileManager.default
            do {
                try fm.removeItem(at: self.foldersInformation[sender.tag].url)
            } catch {
                print("ERROR: \(error)")
            }
            //=====================================================================================================
            
            
            
            //=====================================================================================================
            //MARK:Emptying view
            self.deleteAllConstraints()
            self.everyView.forEach { (view) in
                view.removeFromSuperview()
            }
            self.buttonNames.removeAll()
            self.everyView.removeAll()
            self.foldersInformation.removeAll()
            //=====================================================================================================
            
            
            
            //=====================================================================================================
            //MARK:Repopulate view
            self.readDirectory()
            self.makeUI()
            self.view.setNeedsLayout()
            //=====================================================================================================
        })
        
        //MARK:No statement
        let cancel = UIAlertAction(title: "NO", style: .default)
        
        //MARK:Adding
        alert.addAction(agree)
        alert.addAction(cancel)
        
        //MARK:Showing
        self.present(alert, animated: true, completion: nil)
    }
    
    //sending segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if segue.identifier == "chooseTrack"{
            let nextVC = segue.destination as! listEditorViewController
            nextVC.reciveFolder = sendFolder
        }
    }
    
    //Move to track editor
    @objc func editorAction(sender: UIButton!){
        sendFolder = foldersInformation[sender.tag]
        performSegue(withIdentifier: "chooseTrack", sender: self)
    }
    
    //Export every track to one file
    @objc func createTrack(sender: UIButton!){
        performSegue(withIdentifier: "trackCreator", sender: self)
    }
}

extension UIAlertController {
    //Set background
    func setBackgroundColor(newColor: UIColor) {
        let contentView = self.view.subviews.first?.subviews.first?.subviews.first
        contentView!.backgroundColor = newColor
    }

    //Set title font and title color
    func setTitleColor(newColor: UIColor?) {
        guard let title = self.title else { return }
        let attributeString = NSMutableAttributedString(string: title)
        if let titleColor = newColor {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor], range: NSMakeRange(0, title.count))
        }
        self.setValue(attributeString, forKey: "attributedTitle")
    }

    //Set message color
    func setMessageColor(newColor: UIColor!) {
        guard let message = self.message else { return }
        let attributeString = NSMutableAttributedString(string: message)
        if let messageColorColor = newColor {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor], range: NSMakeRange(0, message.count))
        }
        self.setValue(attributeString, forKey: "attributedMessage")
    }

    //Set tint color of UIAlertController
    func setTintColor(newColor: UIColor) {
        self.view.tintColor = newColor
    }
}
