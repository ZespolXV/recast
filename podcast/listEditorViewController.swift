//
//  listEditorViewController.swift
//  podcast
//
//  Created by Krzysztof on 05/05/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//
import AVFoundation
import Foundation
import UIKit

class listEditorViewController: UIViewController{
    var reciveFolder: FolderInfo = FolderInfo()
    var sendTrack = TrackInfo()
    
    var scrollLayout: ScrollViewCreator!
    var counter = UILabel()
    var error = UILabel()
    var labelCounter = 0

    var buttonNames: [String] = [String]()
    var everyView: [UIView] = [UIView]()
    var trackPack = [TrackInfo]()
    var tmpButtons = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .darkGray
            
        scrollLayout = ScrollViewCreator(superView: self.view)
        let mainView = scrollLayout.mainView
            
        readDirectory()
        makeUI()
            
        for i in everyView{
            mainView.addSubview(i)
        }
            
        scrollLayout.addHorizontalConstrains(everyView, 20.0)
        scrollLayout.addVerticalConstrains(everyView, 15.0, 0.0)
    }
    
    func makeUI(){
        //=========================================================================================
        //MARK:Views
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //=========================================================================================
        
        
        
        //===================================================================================
        //MARK:Logo
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFill
        //===================================================================================
        
        
        
        //=========================================================================================
        //MARK:Folder name label
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = myGray1
        label.text = reciveFolder.name
        label.textAlignment = .center
        label.textColor = .white
        //=========================================================================================
        
        
        
        //=========================================================================================
        //MARK:Error
        error.text = "ERROR: Za malo sciezek do sklejenia!"
        error.translatesAutoresizingMaskIntoConstraints = false
        error.textAlignment = .center
        error.textColor = myRedColor
        error.isHidden = true
        //=========================================================================================
        
        
        
        //=========================================================================================
        //MARK:Button - create
        let createButton = UIButton()
        createButton.translatesAutoresizingMaskIntoConstraints = false
        createButton.setTitle("Create M4A files", for: .normal)
        createButton.setTitleColor(.white, for: .normal)
        createButton.backgroundColor = .gray
        createButton.tag = buttonNames.count
        createButton.addTarget(self, action: #selector(createResult), for: .touchUpInside)
        //=========================================================================================
        
        
        
        //=========================================================================================
        //MARK:Counter
        counter.text = "Amount: \(String(labelCounter))"
        counter.translatesAutoresizingMaskIntoConstraints = false
        counter.textAlignment = .center
        counter.textColor = .white
        //=========================================================================================
        
        
        
        //=========================================================================================
        //MARK:Corners
        createButton.layer.cornerRadius = 10.0
        createButton.layer.masksToBounds = true
        label.layer.cornerRadius = 10.0
        label.layer.masksToBounds = true
        //=========================================================================================
        
        
        //=========================================================================================
        //MARK:Adding
        view.addSubview(createButton)
        view.addSubview(counter)
        //=========================================================================================
        
        
        
        //=========================================================================================
        //MARK:Constraints
        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 80.0),
            
            label.heightAnchor.constraint(equalToConstant: 60.0),
            
            counter.heightAnchor.constraint(equalToConstant: 20.0),
            counter.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10.0),
            counter.topAnchor.constraint(equalTo: createButton.bottomAnchor),
            
            createButton.heightAnchor.constraint(equalToConstant: 60.0),
            createButton.topAnchor.constraint(equalTo: view.topAnchor),
            createButton.widthAnchor.constraint(equalTo: view.widthAnchor),
            
            error.heightAnchor.constraint(equalToConstant: 70.0)
        ])
        //=========================================================================================
        
        
        
        //=========================================================================================
        //MARK:Appending to main view
        everyView.append(imageView)
        everyView.append(label)
        createButtonArray()
        everyView.append(view)
        everyView.append(error)
        //=========================================================================================
    }
    
    func createButtonArray(){
        for (index, i) in trackPack.enumerated(){
            //=====================================================================================
            //MARK:View
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            //=====================================================================================
            
            
                
            //=====================================================================================
            //MARK:Buttons
            let checkbox = UIButton()
            checkbox.translatesAutoresizingMaskIntoConstraints = false
            checkbox.isSelected = false
            checkbox.setTitle("N", for: .normal)
            checkbox.addTarget(self, action: #selector(checkAction), for: .touchUpInside)
            checkbox.tag = index
            checkbox.backgroundColor = myRedColor
                    
            let track = UIButton()
            track.translatesAutoresizingMaskIntoConstraints = false
            track.setTitle(i.name+" PART: "+i.part, for: .normal)
            track.setTitleColor(.white, for: .normal)
            track.backgroundColor = myGray2
            track.tag = index
            track.addTarget(self, action: #selector(editorAction), for: .touchUpInside)
            //=====================================================================================
            
            
            
            //=====================================================================================
            //MARK:Corners
            checkbox.layer.cornerRadius = 10.0
            checkbox.layer.masksToBounds = true
            track.layer.cornerRadius = 10.0
            track.layer.masksToBounds = true
            //=====================================================================================
            
            
            
            //=====================================================================================
            //MARK:Adding
            view.addSubview(track)
            view.addSubview(checkbox)
            //=====================================================================================
            
            
            
            //=====================================================================================
            //MARK:Constraints
            NSLayoutConstraint.activate([
                view.heightAnchor.constraint(equalToConstant: 55.0),
                
                checkbox.topAnchor.constraint(equalTo: view.topAnchor),
                checkbox.widthAnchor.constraint(equalToConstant: 50.0),
                checkbox.heightAnchor.constraint(equalToConstant: 50.0),
                checkbox.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10.0),
                
                track.topAnchor.constraint(equalTo: view.topAnchor),
                track.heightAnchor.constraint(equalToConstant: 50.0),
                track.rightAnchor.constraint(equalTo: checkbox.leftAnchor, constant: -20.0),
                track.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10.0)
            ])
            //=====================================================================================
            
            
            
            //=====================================================================================
            //MARK:Appending
            tmpButtons.append(checkbox)
            everyView.append(view)
            //=====================================================================================
        }
    }
        
    //MARK:Reading documents directory for directories
    func readDirectory(){
        let fm = FileManager.default
        do{
            //For subfolders
            //contents - room folders
            //contFolders - folders inside room folders (e.g 1, 2, 3...)
            //subContents - m4a and txt files
            let contents = try fm.contentsOfDirectory(at: reciveFolder.url, includingPropertiesForKeys: nil)
            let contFolders = contents.filter{ $0.pathExtension == "" }
            contFolders.forEach { (cont) in
                print(cont)
                do{
                    let subContents = try fm.contentsOfDirectory(at: cont, includingPropertiesForKeys: nil)
                    
                    let txtFiles = subContents.filter{ $0.pathExtension == "txt" }
                    let m4aFiles = subContents.filter{ $0.pathExtension == "m4a" }
                                
                    for i in m4aFiles{
                        var tmp = TrackInfo()
                        tmp.name = i.deletingPathExtension().lastPathComponent
                        tmp.urlAudio = i
                        for text in txtFiles {
                            if text.deletingPathExtension().lastPathComponent == i.deletingPathExtension().lastPathComponent{
                                tmp.urlText = text
                            } else if text.deletingPathExtension().pathExtension == "timestamps" {
                                tmp.urlTimestamps = text
                            } else if text.deletingPathExtension().pathExtension == "words" {
                                tmp.urlWords = text
                            } else if text.deletingPathExtension().pathExtension == "duration" {
                                tmp.urlDuration = text
                            }
                            tmp.part = cont.lastPathComponent
                        }
                        trackPack.append(tmp)
                    }
                    trackPack = trackPack.sorted{ $0.part < $1.part }
                } catch {
                    print("Error \(error)")
                }
            }
        } catch {
            print("Error \(error)")
        }
    }
        
    //sending segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "editTrack"{
            let nextVC = segue.destination as! trackViewController
            nextVC.reciveTrack = sendTrack
        }
    }
        
    //Move to track editor
    @objc func editorAction(sender: UIButton!){
        sendTrack = trackPack[sender.tag]
        performSegue(withIdentifier: "editTrack", sender: self)
    }
    
    //MARK:Merging checked tracks
    @objc func createResult(){
        let composition = AVMutableComposition()
        var selectedTracks = [TrackInfo]()

        //Take info from checked buttons
        for i in tmpButtons{
            if i.isSelected == true{
                selectedTracks.append(trackPack[i.tag])
            }
        }
        if selectedTracks.count == 0 {
            print("ERROR: Za malo sciezek do sklejenia!")
            error.isHidden = false
        } else {
            if error.isHidden == false {
                error.isHidden = true
            }
            
            //Prepare tracks
            for i in selectedTracks{
                let compositionAudioTrack = composition.addMutableTrack(withMediaType: .audio,
                                                                        preferredTrackID: CMPersistentTrackID())
                let audio = AVURLAsset(url: i.urlAudio)
                let track = audio.tracks(withMediaType: .audio)
                let audioAssetTrack = track[0]
                let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: audio.duration)

                do{
                    try compositionAudioTrack?.insertTimeRange(timeRange, of: audioAssetTrack, at: CMTime.zero)
                }catch{
                    print(error)
                }
            }
            //Make result file destination
            let fileName = reciveFolder.name+"_RESULT.1.m4a"
            let fileDestinationURL = reciveFolder.url.appendingPathComponent(fileName)
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: fileDestinationURL.path){
                do{
                    print("File exists...")
                    try fileManager.removeItem(at: fileDestinationURL)
                } catch {
                    print("A")
                    print("Error: \(error)")
                }
            }

            //Finishing process
            print("Start merging...")
            let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)
            assetExport!.outputFileType = AVFileType.m4a
            assetExport?.outputURL = fileDestinationURL
            assetExport?.exportAsynchronously(completionHandler: {
                switch assetExport!.status{
                case .failed:
                    print("failed: \(String(describing: assetExport?.error))")
                case .unknown:
                    print("unknown: \(String(describing: assetExport?.error))")
                case .waiting:
                    print("waiting: \(String(describing: assetExport?.error))")
                case .exporting:
                    print("exporting: \(String(describing: assetExport?.error))")
                case .completed:
                    print("completed: \(String(describing: assetExport?.error))")
                case .cancelled:
                    print("cancelled: \(String(describing: assetExport?.error))")
                }
            })
        }
    }
    
    @objc func checkAction(sender: UIButton){
        if sender.isSelected == false {
            sender.setTitle("Y", for: .normal)
            sender.backgroundColor = myGreenColor
            sender.isSelected = true
            labelCounter += 1
        } else {
            sender.setTitle("N", for: .normal)
            sender.backgroundColor = myRedColor
            sender.isSelected = false
            labelCounter -= 1
        }
        counter.text = "Amount: \(String(labelCounter))"
    }
}
