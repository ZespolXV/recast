//
//  joinPopup.swift
//  podcast
//
//  Created by Karol on 15/05/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit

class changeNamePopup: UIViewController{
    override func viewDidLoad() {
        navigationController?.navigationItem.backBarButtonItem?.isEnabled = false;
        
        upView.layer.cornerRadius = 15;
        downView.layer.cornerRadius = 15;
        button1.layer.cornerRadius = 10;
        button2.layer.cornerRadius = 10;
    }
    @IBOutlet weak var downView: UIView!
    @IBOutlet weak var upView: UIView!
    @IBAction func exit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var newName: UITextField!
    @IBOutlet weak var button2: UIButton!
    @IBAction func newName(_ sender: Any) {
        UserDefaults.standard.set(newName.text, forKey: "name")
            dismiss(animated: true, completion: nil)
    }
}

