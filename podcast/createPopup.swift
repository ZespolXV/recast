//
//  createPopup.swift
//  podcast
//
//  Created by Karol on 19/03/2020.
//  Copyright © 2020 ZespolXV. All rights reserved.
//

import Foundation
import UIKit

protocol JoinCreateDelegate {
    func signal(createRoomStatus success: Bool, withComunnicate comunicate:String)
    func signal(joinRoomStatus success: Bool, withComunnicate comunicate:String)
}
extension JoinCreateDelegate{
    func signal(createRoomStatus success: Bool, withComunnicate comunicate:String){
        
    }
    func signal(joinRoomStatus success: Bool, withComunnicate comunicate:String) {
        
    }
}

class createPopup: UIViewController,JoinCreateDelegate,UITextFieldDelegate{
    var room:Room?
    @IBOutlet weak var nameofroom: UITextField!
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    var pomoc = false
    override func viewDidAppear(_ animated: Bool){
        //print("POKAZALEM SIEE!")
        if(pomoc == true){
            pomoc = false
            dismiss(animated: false, completion: nil)
        }
        upView.layer.cornerRadius = 15;
        allView.layer.cornerRadius = 15;
        backButton.layer.cornerRadius = 10;
        createButton.layer.cornerRadius = 10;
        self.nameofroom.delegate = self
    }
    //przesylam instancje room do pokoju
   /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is RecordRoomController{
            let vc = segue.destination as! RecordRoomController
            vc.room = self.room
            print("elko")
        }
    }*/
    
    
    
    @IBAction func create(_ sender: Any) {
        UserDefaults.standard.set(nameofroom.text, forKey: "room")
        //Zakomentowane do testowania
        
        pomoc = true
        
        
        self.room = Room()
        room?.setJoinCreateDelegate(delegate: self)
        self.room?.createRoom(roomID: nameofroom.text)
        
        
    }
    //blokowanie automatycznego przechodzenia do nastepnego ekranu
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    @IBOutlet weak var allView: UIView!
    @IBOutlet weak var upView: UIView!
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    func signal(createRoomStatus success: Bool, withComunnicate comunicate: String) {
        if(success){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondVC = storyboard.instantiateViewController(withIdentifier: "hostRoom") as! RecordRoomController
            // przejscie do nastepnego ekranu
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let secondVC = storyboard.instantiateViewController(withIdentifier: "hostRoom") as! RecordRoomController
            secondVC.room = self.room
            show(secondVC, sender: nil)
            //self.performSegue(withIdentifier: "createToRoom", sender: self)
            secondVC.room = self.room
                     show(secondVC, sender: nil)
        }else{
            let alert = UIAlertController(title: title, message: comunicate/*"Room with this name already exists"*/, preferredStyle: .alert)
            alert.setBackgroundColor(newColor: #colorLiteral(red: 0.2705882353, green: 0.2705882353, blue: 0.2705882353, alpha: 1))
            alert.setTintColor(newColor: .white)
            alert.setMessageColor(newColor: .systemRed)
            if(comunicate == "Check your network connection")
            {
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            }else{
                alert.addAction(UIAlertAction(title: "Okay, I try other name", style: .default, handler: nil))
            }
            present(alert, animated: true, completion: nil)
        }
    }
}
